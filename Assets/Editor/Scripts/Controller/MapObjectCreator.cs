﻿using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using System.Collections.Generic;

public enum CreateAction
{
    StartPoint,
    SavePoint,
    EndPoint,
    AddMapObject,
    AddItemObject,
    CreatePrefab
}


public class MapObjectCreator : EditorWindow
{
    //Prefabs ArrayList
    List<GameObject> prefabs = new List<GameObject>();
    string[] prefabNames;
    List<GameObject> itemprefabs = new List<GameObject>();
    string[] itemprefabNames;
    List<string> mapTypeList = new List<string>();

    //mapobject choice index
    int _choiceIndex;
    int _maptypeIndex;

    //Create New Prefab
    GameObject currObject;
    string nameString = "NewName";

    //Creator Action
    CreateAction currentAction = CreateAction.StartPoint;
    string[] actionStringList;
    int _actionIndex;

    //Parameters
    List<GameObject> savePointList = new List<GameObject>();

    // Add menu named "My Window" to the Window menu
    [MenuItem("Creator/MapObject")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        MapObjectCreator window = (MapObjectCreator)EditorWindow.GetWindow(typeof(MapObjectCreator));
        window.Show();

    }

    void LoadPrefabs()
    {
        prefabs.AddRange(Resources.LoadAll<GameObject>("Prefabs/MapObject"));

        prefabNames = new string[prefabs.Count];
        for (int i = 0; i < prefabs.Count; i++)
        {
            prefabNames[i] = prefabs[i].name;
        }

        itemprefabs.AddRange(Resources.LoadAll<GameObject>("Prefabs/ItemObject"));

        itemprefabNames = new string[itemprefabs.Count];
        for (int i = 0; i < itemprefabs.Count; i++)
        {
            itemprefabNames[i] = itemprefabs[i].name;
        }

        mapTypeList.Add("All");
        mapTypeList.AddRange(System.Enum.GetNames(typeof(MapObjectType)));
    }

    void OnGUI()
    {
        LoadPrefabs();

        EditorUtils.DrawTitle("Scene Setup");
        GUILayout.Label("Select a action");
        if (actionStringList == null)
            actionStringList = System.Enum.GetNames(typeof(CreateAction));

        _actionIndex = EditorGUILayout.Popup(_actionIndex, actionStringList);
        currentAction = (CreateAction)_actionIndex;
        switch (currentAction)
        {
            case CreateAction.StartPoint:
                CreateStartPoint();
                break;

            case CreateAction.SavePoint:
                CreateSavePoint();
                break;

            case CreateAction.EndPoint:
                CreateEndPoint();
                break;

            case CreateAction.CreatePrefab:
                CreatePrefab();
                break;

            case CreateAction.AddMapObject:
                AddMapObject();
                break;

            case CreateAction.AddItemObject:
                AddItemObject();
                break;

        }

        GUILayout.Space(50);
        EditorUtils.DrawSperateLine();
        CheckJobList();
        //groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
        //myBool = EditorGUILayout.Toggle("Toggle", myBool);
        //myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
        //EditorGUILayout.EndToggleGroup();
    }

    void AddMapObject()
    {
        GUILayout.Label("Please select map object");
        GUILayout.Label("MapType:");
        _maptypeIndex = EditorGUILayout.Popup(_maptypeIndex, mapTypeList.ToArray());
        GUILayout.Label("Ref Object:");
        string[] targetPrefabsNames = GetPrefabNamesByMapType(_maptypeIndex);
        GameObject[] targetPrefabs = GetPrefabsByMapType(_maptypeIndex);
        _choiceIndex = EditorGUILayout.Popup(_choiceIndex, targetPrefabsNames);
        if (targetPrefabsNames.Length != 0)
        {
            if (GUILayout.Button("Add " + targetPrefabsNames[_choiceIndex]))
            {
                GameObject newObj = Instantiate(targetPrefabs[_choiceIndex]);
                Selection.activeGameObject = newObj.gameObject;
            }
        }
    }

    void AddItemObject()
    {
        GUILayout.Label("Please select item object");
        GUILayout.Label("Ref Object:");
        _choiceIndex = EditorGUILayout.Popup(_choiceIndex, itemprefabNames);
        if (itemprefabNames.Length != 0)
        {
            if (GUILayout.Button("Add " + itemprefabNames[_choiceIndex]))
            {
                GameObject newObj = Instantiate(itemprefabs[_choiceIndex]);
                Selection.activeGameObject = newObj.gameObject;
            }
        }
    }

    void CreateStartPoint()
    {
        GameObject playerObj = GameObject.FindWithTag("Player");
        if (playerObj)
        {
            GUILayout.Label("- Created Start Point -");
        }
        else
        {

            EditorUtils.DrawWarningLabel("Please Add player object to set the Start Point");
            if (GUILayout.Button("Show Player Folder"))
            {
                Selection.activeObject = Resources.Load("Prefabs/Character/player");
            }
            if (GUILayout.Button("Add Player"))
            {
                Selection.activeObject = Resources.Load("Prefabs/Character/player");
                Instantiate(Selection.activeObject).name = "player";
            }
        }

    }

    void CreateEndPoint()
    {
        GameObject endPointObj = GameObject.FindWithTag("EndPoint");
        if (endPointObj)
        {
            GUILayout.Label("- Created End Point -");
        }
        else
        {
            GUILayout.Label("Please select object for endpoint");
            GUILayout.Label("MapType:");
            _maptypeIndex = EditorGUILayout.Popup(_maptypeIndex, mapTypeList.ToArray());
            GUILayout.Label("Ref Object:");
            string[] targetPrefabsNames = GetPrefabNamesByMapType(_maptypeIndex);
            GameObject[] targetPrefabs = GetPrefabsByMapType(_maptypeIndex);
            _choiceIndex = EditorGUILayout.Popup(_choiceIndex, targetPrefabsNames);
            if (targetPrefabsNames.Length != 0)
            {
                if (GUILayout.Button("Add " + targetPrefabsNames[_choiceIndex]))
                {
                    endPointObj = Instantiate(targetPrefabs[_choiceIndex]);
                    endPointObj.name = "EndPoint";
                    endPointObj.tag = "EndPoint";
                    Selection.activeGameObject = endPointObj.gameObject;
                }
            }          
        }
    }

    void CreateSavePoint()
    {
        GUILayout.Label("Please select object for savepoint");
        GUILayout.Label("MapType:");
        _maptypeIndex = EditorGUILayout.Popup(_maptypeIndex, mapTypeList.ToArray());
        GUILayout.Label("Ref Object:");
        string[] targetPrefabsNames = GetPrefabNamesByMapType(_maptypeIndex);
        GameObject[] targetPrefabs = GetPrefabsByMapType(_maptypeIndex);
        _choiceIndex = EditorGUILayout.Popup(_choiceIndex, targetPrefabsNames);
        if (targetPrefabsNames.Length != 0)
        {
            if (GUILayout.Button("Add " + targetPrefabsNames[_choiceIndex]))
            {
                GameObject newSavePoint = Instantiate(targetPrefabs[_choiceIndex]);
                newSavePoint.name = "SavePoint" + savePointList.Count;
                newSavePoint.tag = "SavePoint";
                Selection.activeGameObject = newSavePoint.gameObject;

                savePointList.Add(newSavePoint);
            }
        }
    }

    void CreatePrefab()
    {
        GUILayout.Label("Please select object to clone");
        _choiceIndex = EditorGUILayout.Popup(_choiceIndex, prefabNames);
        GUILayout.Label("Create Settings", EditorStyles.boldLabel);
        nameString = EditorGUILayout.TextField("name", nameString);
        if (GUILayout.Button("Create New Prefab"))
        {
            CreatePrefabToProject(prefabs[_choiceIndex]);
        }
    }

    void CreatePrefabToProject(GameObject sampleObj)
    {
        Debug.Log("CreatePrefabToProject: " + nameString);

        // Set the path as within the Assets folder,
        // and name it as the GameObject's name with the .Prefab format
        string localPath = "Assets/Resources/Prefabs/MapObject/" + nameString + ".prefab";

        // Make sure the file name is unique, in case an existing Prefab has the same name.
        localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);

        // Create the new Prefab.
        GameObject newObject = Instantiate(sampleObj);
        GameObject prefabObject = PrefabUtility.SaveAsPrefabAssetAndConnect(newObject, localPath, InteractionMode.UserAction);

        newObject.name = prefabObject.name;

        currObject = newObject;
    }

    void CheckJobList()
    {
        EditorUtils.DrawTitle("CheckList");

        if (GameObject.FindWithTag("Player"))
        {
            GUILayout.Label("- Created Start Point -");
        }
        else
        {
            EditorUtils.DrawWarningLabel("Start Point Missing!!");
        }

        if (GameObject.FindGameObjectsWithTag("SavePoint").Length == 0)
        {
            EditorUtils.DrawWarningLabel("No Save Point !!");
        }
        else
        {
            GUILayout.Label("- " + GameObject.FindGameObjectsWithTag("SavePoint").Length + " Save Point -");
        }

        if (GameObject.FindWithTag("EndPoint"))
        {
            GUILayout.Label("- Created End Point -");
        }
        else
        {
            EditorUtils.DrawWarningLabel("End Point Missing!!");
        }

        GameObject[] doorList = GameObject.FindGameObjectsWithTag("Door");
        for (int i = 0; i < doorList.Length; i++)
        {
            //if (doorList[i].GetComponent<MapObject>().needItemToOpen == null)
                //EditorUtils.DrawWarningLabel(doorList[i].name  + "needItemToOpen is Null");
        }
    }

    GameObject[] GetPrefabsByMapType(int maptypeIndex)
    {
        return prefabs.FindAll(x => x.tag == System.Enum.GetName(typeof(MapObjectType), maptypeIndex - 1)).ToArray();
    }

    string[] GetPrefabNamesByMapType(int maptypeIndex)
    {
        if (maptypeIndex == 0)
            return prefabNames;

        List<GameObject> tempList = prefabs.FindAll(x => x.tag == System.Enum.GetName(typeof(MapObjectType), maptypeIndex - 1));
        string[] strList = new string[tempList.Count];

        for (int i = 0; i < tempList.Count; i++)
        {
            strList[i] = tempList[i].name;
        }
        return strList;
    }
}