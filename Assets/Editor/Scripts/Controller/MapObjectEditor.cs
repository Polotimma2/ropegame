﻿using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using System.Collections.Generic;

public enum MapObjectType
{
    Wind,
    Cannon,
    Teleport,
    StartPoint,
    SavePoint,
    EndPoint,
    RandomDisappear,
    Breakable,
    Trigger,
    Jumpbox
}

public enum CreateObjectType
{
    Terrain,
    MabObject,
    Item
}

public enum CreateTerrainType
{
    Ground,
    Wall,
    Ice,
    Spike,
    Eletic,
    Sticky
}

public enum CreateTerrainMechanicType {
    Move,
    Rotate,
    Breakable
}

public enum CreateMabObjectType {
    StartPoint,
    SavePoint,
    EndPoint,
    Wind,
    Cannon,
    Teleport,
    Trigger,
    Jumpbox,
    CrashBox
}

public enum CreateItemType {
    Key
}

public class MapObjectEditor : EditorWindow
{
    #region creator
    private List<string> txtCreatorObject = new List<string>();
    private int selectedCreateObject = 0;
    #endregion

    private MapObjectEditor window;

    #region Creator Component
    private TerrainCreatorComponent terrainCC;
    private MapObjectCreatorComponent mapObjCC;
    private ItemCreatorComponent itemCC;
    #endregion

    // Add menu named "My Window" to the Window menu
    [MenuItem("Creator/MapObject")]


    static void Init()
    {
        // Get existing open window or if none, make a new one:
        MapObjectEditor window = (MapObjectEditor)GetWindow(typeof(MapObjectEditor));
        window.Show();

    }

    public void InitSelection()
    {
        txtCreatorObject = new List<string>();
        txtCreatorObject.AddRange(System.Enum.GetNames(typeof(CreateObjectType)));

        terrainCC = new TerrainCreatorComponent();
        terrainCC.InitSelection();
        mapObjCC = new MapObjectCreatorComponent();
        mapObjCC.InitSelection();
        itemCC = new ItemCreatorComponent();
        itemCC.InitSelection();
    }

    void OnGUI()
    {
        if (terrainCC == null)
            InitSelection();

        EditorUtils.DrawTitle("Selecting Create Object Type : " + txtCreatorObject[selectedCreateObject]);

        GUILayout.BeginHorizontal();
        selectedCreateObject = GUILayout.Toolbar(selectedCreateObject, txtCreatorObject.ToArray());
        GUILayout.EndHorizontal();

        GUILayout.Space(10);

        switch (selectedCreateObject)
        {
            case (int)CreateObjectType.Terrain:
                terrainCC.DrawCreatorTerrainMode();
                break;
            case (int)CreateObjectType.MabObject:
                mapObjCC.DrawCreatorMapObjectMode();
                break;
            case (int)CreateObjectType.Item:
                itemCC.DrawCreatorItemMode();
                break;
            default:
                break;
        }
    }

    #region Editor
    void DrawEditorMode() { }
    #endregion
}