﻿#if UNITY_EDITOR 
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
public static class EditorUtils
{
    public static void DrawTitle(string content)
    {
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(content, EditorStyles.boldLabel);
        GUILayout.EndHorizontal();
    }

    public static void DrawSubTitle(string title) {
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(">>>" + title, EditorStyles.boldLabel);
        GUILayout.EndHorizontal();
    }

    public static void DrawSperateLine()
    {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("=======================================================================================", EditorStyles.boldLabel);
        GUILayout.EndHorizontal();
    }

    public static void DrawWarningLabel(string content)
    {
        GUILayout.BeginHorizontal();
        GUIStyle s = new GUIStyle(EditorStyles.textField);
        s.normal.textColor = Color.red;

        GUILayout.Label(content, s);
        GUILayout.EndHorizontal();
    }

    public static GameObject CreatePrefabToProject(GameObject sampleObj)
    {
        Debug.Log("CreatePrefabToProject: " + sampleObj.name);

        // Set the path as within the Assets folder,
        // and name it as the GameObject's name with the .Prefab format
        string localPath = "Assets/Resources/Prefabs/MapObject/" + sampleObj.name + ".prefab";

        // Make sure the file name is unique, in case an existing Prefab has the same name.
        localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);

        // Create the new Prefab.
        GameObject newObject = GameObject.Instantiate(sampleObj);
        GameObject prefabObject = PrefabUtility.SaveAsPrefabAssetAndConnect(newObject, localPath, InteractionMode.UserAction);

        newObject.name = prefabObject.name;

        return newObject;
    }

    public static GameObject[] GetPrefabsByType<T>(List<GameObject> prefabs)
    {
        return prefabs.FindAll(x => x.GetComponent<T>() != null).ToArray();
    }

    public static GameObject[] GetPrefabsByName(List<GameObject> prefabs, string containName)
    {
        return prefabs.FindAll(x => x.name.Contains(containName)).ToArray();
    }

    public static string[] GetGameNames(GameObject[] inputList)
    {
        string[] strList = new string[inputList.Length];

        for (int i = 0; i < inputList.Length; i++)
        {
            strList[i] = inputList[i].name;
        }
        return strList;
    }

    public static int TryGetUnityObjectsOfTypeFromPath<T>(string path, List<T> assetsFound) where T : UnityEngine.Object
    {
        string[] filePaths = System.IO.Directory.GetFiles(path);

        int countFound = 0;

        if (filePaths != null && filePaths.Length > 0)
        {
            for (int i = 0; i < filePaths.Length; i++)
            {
                UnityEngine.Object obj = UnityEditor.AssetDatabase.LoadAssetAtPath(filePaths[i], typeof(T));
                if (obj is T asset)
                {
                    countFound++;
                    if (!assetsFound.Contains(asset))
                    {
                        assetsFound.Add(asset);
                    }
                }
            }
        }
        return countFound;
    }

    public static T[] DrawObjectList<T>(List<T> list, string titlename) where T : class
    {
        int newCount = Mathf.Max(0, EditorGUILayout.IntField(titlename + " size: ", list.Count));
        while (newCount < list.Count)
            list.RemoveAt(list.Count - 1);
        while (newCount > list.Count)
        {

            list.Add(default(T));
        }

        List<Object> tempList = new List<Object>();
        for(int i = 0; i < list.Count; i++)
        {
            tempList.Add(list[i] as Object);
        }

        for (int i = 0; i < list.Count; i++)
        {
            tempList[i] = (Object)EditorGUILayout.ObjectField(tempList[i], typeof(Object), true);
        }

        for (int i = 0; i < list.Count; i++)
        {
                list[i] = tempList[i] as T;
        }

        return list.ToArray();
    }
}
#endif