﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ItemCreatorComponent 
{
    private CreateItemType selectedItemType;
    private CreateItemType beforeSelectedItemType;

    private List<GameObject> prefabs;
    private string[] prefabNames;

    private bool changedItemType;
    private int _choiceIndex;
    private int _beforeChoiceIndex;

    private GameObject createdObj;
    public void InitSelection()
    {
        prefabs = new List<GameObject>();
        EditorUtils.TryGetUnityObjectsOfTypeFromPath<GameObject>("Assets/Prefabs/ItemObject", prefabs);

        prefabNames = new string[prefabs.Count];
        for (int i = 0; i < prefabs.Count; i++)
        {
            prefabNames[i] = prefabs[i].name;
        }
    }

    public void DrawCreatorItemMode()
    {
        EditorUtils.DrawTitle("Step 1. Select Item");

        GUILayout.BeginHorizontal();
        selectedItemType = (CreateItemType)EditorGUILayout.EnumPopup(selectedItemType);
        GUILayout.EndHorizontal();
        if (beforeSelectedItemType != selectedItemType)
        {
            changedItemType = true;
            beforeSelectedItemType = selectedItemType;
            _choiceIndex = 0;
            _beforeChoiceIndex = -1;
        }

        GUILayout.Space(10);
        EditorUtils.DrawTitle("Step 2. Sample Prefab");
        GameObject[] targetPrefabs = GetTargetPrefabs();

        if (targetPrefabs == null)
            return;

        string[] targetPrefabsNames = EditorUtils.GetGameNames(targetPrefabs);
        _choiceIndex = EditorGUILayout.Popup(_choiceIndex, targetPrefabsNames);

        if (targetPrefabsNames.Length != 0)
        {
            if (_beforeChoiceIndex != _choiceIndex)
            {

            }

            EditorUtils.DrawTitle("Step 3. Create Prefab");
            if (GUILayout.Button("Add " + targetPrefabsNames[_choiceIndex]))
            {
                GameObject newObj = GameObject.Instantiate(targetPrefabs[_choiceIndex]);
                createdObj = newObj;
                Selection.activeGameObject = newObj.gameObject;

            }
            _beforeChoiceIndex = _choiceIndex;
        }
    }

    private GameObject[] GetTargetPrefabs()
    {
        switch (selectedItemType)
        {
            case CreateItemType.Key:
                return EditorUtils.GetPrefabsByName(prefabs, "key");

        }

        return null;
    }
 }
