﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
public class MapObjectCreatorComponent
{
    private CreateMabObjectType selectedMapObjectType;
    private CreateMabObjectType beforeSelectedMapObjectType;
    private bool changedMapObjectType;

    private List<GameObject> prefabs;
    private string[] prefabNames;

    private int _choiceIndex;
    private int _beforeChoiceIndex;
    private GameObject createdObject;

    private SwitchType _leftSwitch;
    private DirectionType _leftdirectionType;

    private float _triggerTime;
    private float _repawnTime;
    private Sprite[] _spriteList = new Sprite[0];
    private float _switchTime;
    private Vector3 _addForceDirection;
    private Vector3 _secondAddForceDirection;
    private bool _canStartOn;
    private bool _isTriggerLeft;
    private float _effectByGravity;


    public void InitSelection()
    {
        LoadPrefabs();
    }

    public void LoadPrefabs()
    {
        prefabs = new List<GameObject>();
        EditorUtils.TryGetUnityObjectsOfTypeFromPath<GameObject>("Assets/Prefabs/MapObject", prefabs);

        prefabNames = new string[prefabs.Count];
        for (int i = 0; i < prefabs.Count; i++)
        {
            prefabNames[i] = prefabs[i].name;
        }
    }
    public void DrawCreatorMapObjectMode()
    {
        EditorUtils.DrawTitle("Step 1. Select Map Object Type");

        GUILayout.BeginHorizontal();
        selectedMapObjectType = (CreateMabObjectType)EditorGUILayout.EnumPopup(selectedMapObjectType);
        if(beforeSelectedMapObjectType != selectedMapObjectType)
        {
            changedMapObjectType = true;
            beforeSelectedMapObjectType = selectedMapObjectType;
            _choiceIndex = 0;
            _beforeChoiceIndex = -1;
        }

        GUILayout.EndHorizontal();

        if (selectedMapObjectType < 0)
            return;

        EditorUtils.DrawTitle("Step 2. Sample Prefab");
        GameObject[] targetPrefabs = GetTargetPrefabs();

        if (targetPrefabs == null)
            return;

        string[] targetPrefabsNames = EditorUtils.GetGameNames(targetPrefabs);

        _choiceIndex = EditorGUILayout.Popup(_choiceIndex, targetPrefabsNames);
        if (targetPrefabsNames.Length != 0)
        {
            EditorUtils.DrawTitle("Step 3. Prefab Setting");
            Mechanic mechanic = targetPrefabs[_choiceIndex].GetComponent<Mechanic>();

            if (_beforeChoiceIndex != _choiceIndex)
            {
                _leftSwitch = mechanic.leftSwitch;
                _leftdirectionType = mechanic.leftDirection;
            }

            //Common Setting
            EditorUtils.DrawSubTitle("Trigger part");
            EditorGUILayout.LabelField("Only use for trigger object, ignore if no use", EditorStyles.helpBox);

            EditorGUILayout.LabelField("Left Switch:");
            _leftSwitch = (SwitchType)EditorGUILayout.EnumPopup(_leftSwitch);
            EditorGUILayout.LabelField("Left Direction:");
            _leftdirectionType = (DirectionType)EditorGUILayout.EnumPopup(_leftdirectionType);

            //Crashbox
            if (selectedMapObjectType == CreateMabObjectType.CrashBox)
            {
                if (_beforeChoiceIndex != _choiceIndex)
                { 
                    _switchTime = ((MAutoCrash)mechanic).switchSpriteTime;
                    _triggerTime = ((MAutoCrash)mechanic).triggerTime;
                    _repawnTime = ((MAutoCrash)mechanic).respawnTime;
                    _spriteList = ((MAutoCrash)mechanic).crashSpriteList;
                }

                _triggerTime = float.Parse(EditorGUILayout.TextField("Trigger Time:: ", _triggerTime.ToString()));
                _repawnTime = float.Parse(EditorGUILayout.TextField("Repawn Time:: ", _repawnTime.ToString()));
                _spriteList = EditorUtils.DrawObjectList(_spriteList.ToList(), "Sprite List");
                _switchTime = float.Parse(EditorGUILayout.TextField("Sprite Switch Time:: ", _switchTime.ToString()));
            }

            //Jumpbox
            else if (selectedMapObjectType == CreateMabObjectType.Jumpbox)
            {
                if (_beforeChoiceIndex != _choiceIndex)
                {
                    _addForceDirection = ((MJumpbox)mechanic).addForce;
                    _spriteList = ((MJumpbox)mechanic).triggerSprites;
                    _switchTime = ((MJumpbox)mechanic).switchSpriteTime;
                }

                _addForceDirection = EditorGUILayout.Vector3Field("Add Force Direction: ", _addForceDirection);
                _spriteList = EditorUtils.DrawObjectList(_spriteList.ToList(), "Trigger Sprite List");
                _switchTime = float.Parse(EditorGUILayout.TextField("Sprite Switch Time:: ", _switchTime.ToString()));
            }

            //Cannon
            else if (selectedMapObjectType == CreateMabObjectType.Cannon)
            {
                if (_beforeChoiceIndex != _choiceIndex)
                {
                    _addForceDirection = ((MCannon)mechanic).addForce;
                    _secondAddForceDirection = ((MCannon)mechanic).secondAddForce;
                    _triggerTime = ((MCannon)mechanic).looptime;
                    _repawnTime = ((MCannon)mechanic).cooldowntime;
                    _canStartOn = ((MCannon)mechanic).canFire;
                    _isTriggerLeft = ((MCannon)mechanic).isTriggerLeft;
                    _switchTime = ((MCannon)mechanic).bullet.GetComponent<MBullet>().autoDestryTime;
                    _effectByGravity = ((MCannon)mechanic).bullet.GetComponent<Rigidbody2D>().gravityScale;
                }

                EditorUtils.DrawSubTitle("Cannon part");
                _addForceDirection = EditorGUILayout.Vector3Field("Add Force Direction: ", _addForceDirection);
                _secondAddForceDirection = EditorGUILayout.Vector3Field("Second Add Force Direction: ", _secondAddForceDirection);
                _triggerTime = float.Parse(EditorGUILayout.TextField("Loop Time: ", _triggerTime.ToString()));
                _repawnTime = float.Parse(EditorGUILayout.TextField("Cooldown Time: ", _repawnTime.ToString()));
                _canStartOn = EditorGUILayout.Toggle("Can Start On Fire", _canStartOn);
                _isTriggerLeft = EditorGUILayout.Toggle("Is Trigger Left", _isTriggerLeft);

                EditorUtils.DrawSubTitle("Bullet part");
                _switchTime = float.Parse(EditorGUILayout.TextField("Auto Destory Time: ", _switchTime.ToString()));
                _effectByGravity = float.Parse(EditorGUILayout.TextField("Bullet Gravity Scale: ", _effectByGravity.ToString()));
            }

            //Wind
            else if (selectedMapObjectType == CreateMabObjectType.Wind)
            {
                if (_beforeChoiceIndex != _choiceIndex)
                {
                    _triggerTime = ((MWind)mechanic).force20;
                    _repawnTime = ((MWind)mechanic).force2000;
                    _switchTime = ((MWind)mechanic).GetComponent<AreaEffector2D>().forceAngle;
                }

                _triggerTime = float.Parse(EditorGUILayout.TextField("Force 20: ", _triggerTime.ToString()));
                _repawnTime = float.Parse(EditorGUILayout.TextField("Force 2000: ", _repawnTime.ToString()));
                _switchTime = float.Parse(EditorGUILayout.TextField("Force Angle: ", _switchTime.ToString()));
            }

            EditorUtils.DrawTitle("Step 4. Create Prefab");

            if (GUILayout.Button("Add " + targetPrefabsNames[_choiceIndex]))
            {
                GameObject newObj = GameObject.Instantiate(targetPrefabs[_choiceIndex]);
                newObj.GetComponent<Mechanic>().leftSwitch = _leftSwitch;
                newObj.GetComponent<Mechanic>().leftDirection = _leftdirectionType;

                createdObject = newObj;
                Selection.activeGameObject = newObj.gameObject;

                if(selectedMapObjectType == CreateMabObjectType.CrashBox)
                {
                    MAutoCrash mac = createdObject.GetComponent<MAutoCrash>();
                    mac.switchSpriteTime = _switchTime;
                    mac.crashSpriteList = _spriteList;
                    mac.triggerTime = _triggerTime;
                    mac.respawnTime = _repawnTime;
                }
                else if (selectedMapObjectType == CreateMabObjectType.Jumpbox)
                {
                    MJumpbox mac = createdObject.GetComponent<MJumpbox>();
                    mac.switchSpriteTime = _switchTime;
                    mac.triggerSprites = _spriteList;
                    mac.addForce = _addForceDirection;
                }

               else  if (selectedMapObjectType  == CreateMabObjectType.Teleport)
                {
                    GameObject secondTeleport = GameObject.Instantiate(targetPrefabs[_choiceIndex]);
                    newObj.GetComponent<MTeleport>().teleportTo = secondTeleport.GetComponent<MTeleport>();
                    secondTeleport.GetComponent<MTeleport>().teleportTo = newObj.GetComponent<MTeleport>().teleportTo;
                }

                else if(selectedMapObjectType == CreateMabObjectType.Cannon)
                {
                    MCannon cannon = createdObject.GetComponent<MCannon>();
                    cannon.addForce = _addForceDirection;
                    cannon.secondAddForce = _secondAddForceDirection;
                    cannon.looptime = _triggerTime;
                    cannon.cooldowntime = _repawnTime;
                    cannon.canFire = _canStartOn;
                    cannon.isTriggerLeft = _isTriggerLeft;
                    cannon.bullet.GetComponent<MBullet>().autoDestryTime = _switchTime;
                    cannon.bullet.GetComponent<Rigidbody2D>().gravityScale = _effectByGravity;
                }

                else if (selectedMapObjectType == CreateMabObjectType.Wind)
                {
                    MWind wind = createdObject.GetComponent<MWind>();
                    wind.force20 = _triggerTime;
                    wind.force2000 = _repawnTime;
                    wind.GetComponent<AreaEffector2D>().forceAngle = _switchTime;
                    
                }
            }
            _beforeChoiceIndex = _choiceIndex;
        }
    }

    private GameObject[] GetTargetPrefabs()
    {
        switch (selectedMapObjectType)
        {
            case CreateMabObjectType.Wind:
                return EditorUtils.GetPrefabsByType<MWind>(prefabs);

            case CreateMabObjectType.CrashBox:
                return EditorUtils.GetPrefabsByType<MAutoCrash>(prefabs);

            case CreateMabObjectType.Cannon:
                return EditorUtils.GetPrefabsByType<MCannon>(prefabs);

            case CreateMabObjectType.Jumpbox:
                return EditorUtils.GetPrefabsByType<MJumpbox>(prefabs);

            case CreateMabObjectType.EndPoint:
                return EditorUtils.GetPrefabsByType<MEndPoint>(prefabs);

            case CreateMabObjectType.SavePoint:
                return EditorUtils.GetPrefabsByType<MSavePoint>(prefabs);

            case CreateMabObjectType.StartPoint:
                return EditorUtils.GetPrefabsByType<MStartPoint>(prefabs);

            case CreateMabObjectType.Teleport:
                return EditorUtils.GetPrefabsByType<MTeleport>(prefabs);

            case CreateMabObjectType.Trigger:
                return EditorUtils.GetPrefabsByType<MTrigger>(prefabs);
        }

        return null;
    }
}
