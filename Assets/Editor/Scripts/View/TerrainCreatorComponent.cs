﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System;
using System.Linq;

public  class TerrainCreatorComponent
{
    // Terrain  
    private CreateTerrainType selectedTerrainType;
    private CreateTerrainType lastSelectedTerrainType;

    // Mechanic
    private Dictionary<CreateTerrainMechanicType, bool> dictTerrainMechanic = new Dictionary<CreateTerrainMechanicType, bool>();

    // Prefabs
    private List<GameObject> prefabs;
    private string[] prefabNames;
    private int selectedPrefabIndex;
    private int lastSelectedPrefabIndex;
    private GameObject createdObject;

    // Setting
    private bool openTriggerSetting = false;

    private SwitchType _sfLeftSwitch; //surface feature left switch

    private SwitchType _leftSwitch; // mechanic left switch
    private DirectionType _leftdirectionType; // mechanic left direction

    public UnityEngine.Object _parentObj; //Sticky
    public UnityEngine.Object _childObj; //Sticky

    public UnityEngine.Object _targetTerain;
    public DirectionType _mrDefaultDirection = DirectionType.Right;
    public float _mrMoveSpeed;

    private float _moveSpeed; //MovePath
    private bool _isAutoStart; //MovePath

    private Vector2 _rotateDirection; //MoveRotate
    private float _rotateAngle; //MoveRotate
    private Vector2 _rotateSndDirection; //MoveRotate
    private float _rotateSndAngle; //MoveRotate

    private float _triggerTime;
    private float _repawnTime;
    private Sprite[] _spriteList = new Sprite[0];
    private float _switchTime;



    public void InitSelection()
    {
        prefabs = new List<GameObject>();

        prefabs = new List<GameObject>();
        EditorUtils.TryGetUnityObjectsOfTypeFromPath<GameObject>("Assets/Prefabs/Terrain", prefabs);

        prefabNames = new string[prefabs.Count];
        for (int i = 0; i < prefabs.Count; i++)
        {
            prefabNames[i] = prefabs[i].name;
        }

        dictTerrainMechanic.Clear();
        foreach (CreateTerrainMechanicType type in Enum.GetValues(typeof(CreateTerrainMechanicType))) {
            dictTerrainMechanic.Add(type, false);
        }
    }
    public void DrawCreatorTerrainMode()
    {
        EditorUtils.DrawTitle("Step 1. Select Terrain");

        GUILayout.BeginHorizontal();
        selectedTerrainType = (CreateTerrainType)EditorGUILayout.EnumPopup(selectedTerrainType);
        GUILayout.EndHorizontal();

        // Detect if choise changed
        if (selectedTerrainType != lastSelectedTerrainType)
        {
            lastSelectedTerrainType = selectedTerrainType;
            selectedPrefabIndex = 0;
            lastSelectedPrefabIndex = -1;
        }

        EditorUtils.DrawTitle("Step 2. Select Prefab");
        GameObject[] targetPrefabs = GetTargetPrefabs();

        if (targetPrefabs == null)
            return;

        string[] targetPrefabsNames = EditorUtils.GetGameNames(targetPrefabs);
        selectedPrefabIndex = EditorGUILayout.Popup(selectedPrefabIndex, targetPrefabsNames);

        if (targetPrefabsNames[selectedPrefabIndex] == "MovingSpike" || targetPrefabsNames[selectedPrefabIndex] == "MovingElectric")
        {
            EditorUtils.DrawTitle("Step 3. Moving Setting");

            if (lastSelectedPrefabIndex != selectedPrefabIndex)
            {
                MovePathnRotate mr = targetPrefabs[selectedPrefabIndex].GetComponent<MovePathnRotate>();

                _mrDefaultDirection = mr.defaultDirection;
                _mrMoveSpeed = mr.moveSpeed;
            }

            _targetTerain = EditorGUILayout.ObjectField("Add to Terrain :", _targetTerain, typeof(UnityEngine.Object), true);
            if (_targetTerain == null)
            {
                GUIStyle s = new GUIStyle(EditorStyles.label);
                s.normal.textColor = Color.red;
                EditorGUILayout.LabelField(" ***** YOU MUST select the Target Terrain *****", s);
                GUI.contentColor = Color.white;
            }
            _mrDefaultDirection = (DirectionType)EditorGUILayout.EnumPopup("Default Direction :", _mrDefaultDirection);
            _mrMoveSpeed = EditorGUILayout.DelayedFloatField("Move Speed ", _mrMoveSpeed);
        }
        else if (selectedTerrainType == CreateTerrainType.Sticky) {
            EditorUtils.DrawTitle("Step 3. Sticky Setting");
            _parentObj = EditorGUILayout.ObjectField("Parent Object", _parentObj, typeof(UnityEngine.Object), true);
            _childObj = EditorGUILayout.ObjectField("Children Object", _childObj, typeof(UnityEngine.Object), true);
        }
        else
        {
            EditorUtils.DrawTitle("Step 3. Select Mechanic (Optional & Multipule)");

            foreach (CreateTerrainMechanicType type in Enum.GetValues(typeof(CreateTerrainMechanicType)))
            {
                dictTerrainMechanic[type] = EditorGUILayout.Toggle(type.ToString(), dictTerrainMechanic[type]);
            }
        }

        EditorUtils.DrawTitle("Step 4. Mechanic Setting");

        foreach (KeyValuePair<CreateTerrainMechanicType, bool> p in dictTerrainMechanic)
        {
            if (p.Value)
            {
                DrawMechanicSetting(p.Key);
            }
        }

        // reset setting if prefab changed
        if (lastSelectedPrefabIndex != selectedPrefabIndex)
        {
            SurfaceFeature sf = targetPrefabs[selectedPrefabIndex].GetComponent<SurfaceFeature>();
            _sfLeftSwitch = sf.leftSwitch;
        }

        GUILayout.Space(10);
        openTriggerSetting = EditorGUILayout.Toggle("Open Trigger Setting", openTriggerSetting);

        if (openTriggerSetting)
        {
            EditorUtils.DrawSubTitle("if trigger interact w Terrain");
            EditorGUILayout.LabelField("Left/Top (Switch Mode) :");
            _sfLeftSwitch = (SwitchType)EditorGUILayout.EnumPopup(_sfLeftSwitch);
            foreach (KeyValuePair<CreateTerrainMechanicType, bool> p in dictTerrainMechanic)
            {
                if (p.Value)
                {
                    DrawMechanicTriggerSetting(p.Key);
                }
            }
        }

        EditorUtils.DrawTitle("Step 5. Create Terrain");
        if (GUILayout.Button("Add Terrain (" + selectedTerrainType.ToString() + ")"))
        {

            if (targetPrefabsNames[selectedPrefabIndex] == "MovingSpike" || targetPrefabsNames[selectedPrefabIndex] == "MovingElectric") {
                if (_targetTerain != null) {
                    GameObject newObj = GameObject.Instantiate(targetPrefabs[selectedPrefabIndex]);
                    newObj.GetComponent<SurfaceFeature>().leftSwitch = _sfLeftSwitch;
                    newObj.transform.parent = (_targetTerain as GameObject).transform;
                    newObj.transform.localPosition = Vector3.zero;

                    createdObject = newObj;
                    Selection.activeGameObject = newObj.gameObject;

                    MovePathnRotate moveMechanic = newObj.GetComponent<MovePathnRotate>();
                    moveMechanic.defaultDirection = _mrDefaultDirection;
                    moveMechanic.moveSpeed = _mrMoveSpeed;
                }
            } else
            {
                GameObject newObj = GameObject.Instantiate(targetPrefabs[selectedPrefabIndex]);
                newObj.GetComponent<SurfaceFeature>().leftSwitch = _sfLeftSwitch;

                createdObject = newObj;
                Selection.activeGameObject = newObj.gameObject;

                if (selectedTerrainType == CreateTerrainType.Sticky)
                {
                    SFSticky sticky = newObj.GetComponent<SFSticky>();
                    sticky.parentObj = _parentObj as GameObject;
                    sticky.childrenObj = _childObj as GameObject;
                }

                foreach (KeyValuePair<CreateTerrainMechanicType, bool> p in dictTerrainMechanic)
                {
                    if (p.Value)
                    {
                        switch (p.Key)
                        {
                            case CreateTerrainMechanicType.Move:
                                MovePath moveMechanic = newObj.AddComponent<MovePath>();
                                moveMechanic.moveSpeedPos = _moveSpeed;
                                moveMechanic.isAutoStart = _isAutoStart;
                                break;
                            case CreateTerrainMechanicType.Rotate:
                                MoveRotate rotateMechanic = newObj.AddComponent<MoveRotate>();
                                rotateMechanic.direction = _rotateDirection;
                                rotateMechanic.angle = _rotateAngle;
                                if (openTriggerSetting)
                                {
                                    rotateMechanic.secondDirection = _rotateSndDirection;
                                    rotateMechanic.secondAngle = _rotateAngle;
                                }
                                break;
                            case CreateTerrainMechanicType.Breakable:
                                MAutoCrash crashMechanic = newObj.AddComponent<MAutoCrash>();
                                crashMechanic.triggerTime = _triggerTime;
                                crashMechanic.respawnTime = _repawnTime;
                                crashMechanic.switchSpriteTime = _switchTime;
                                crashMechanic.crashSpriteList = _spriteList;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

            

        lastSelectedPrefabIndex = selectedPrefabIndex;
        lastSelectedTerrainType = selectedTerrainType;
    }

    private void DrawMechanicSetting(CreateTerrainMechanicType type) {

        switch (type)
        {
            case CreateTerrainMechanicType.Move:
                EditorUtils.DrawSubTitle("Move Setting");

                _moveSpeed = EditorGUILayout.DelayedFloatField("Move Speed ", _moveSpeed);
                _isAutoStart = EditorGUILayout.Toggle("Is Auto Start ", _isAutoStart);
                break;
            case CreateTerrainMechanicType.Rotate:
                EditorUtils.DrawSubTitle("Rotate Setting");

                _rotateDirection = EditorGUILayout.Vector3Field("Rotate Direction", _rotateDirection);
                _rotateAngle = EditorGUILayout.DelayedFloatField("Rotate Angle", _rotateAngle);
                break;
            case CreateTerrainMechanicType.Breakable:
                EditorUtils.DrawSubTitle("Breakable Setting");

                _triggerTime = EditorGUILayout.DelayedFloatField("Trigger Time ", _triggerTime);
                _repawnTime = EditorGUILayout.DelayedFloatField("Repawn Time ", _repawnTime);
                _switchTime = EditorGUILayout.DelayedFloatField("Sprite Switch Time ", _switchTime);
                _spriteList = EditorUtils.DrawObjectList(_spriteList.ToList(), "Sprite List");
                break;
        }
    }

    private void DrawMechanicTriggerSetting(CreateTerrainMechanicType type)
    {
        switch (type)
        {
            case CreateTerrainMechanicType.Rotate:
                EditorUtils.DrawSubTitle("if trigger interact w Mechanic");

                _rotateSndDirection = EditorGUILayout.Vector3Field("Second Rotate Direction", _rotateSndDirection);
                _rotateSndAngle = EditorGUILayout.DelayedFloatField("Second Rotate Angle", _rotateSndAngle);
                break;
        }
    }

    private GameObject[] GetTargetPrefabs()
    {
        switch (selectedTerrainType)
        {
            case CreateTerrainType.Ground:
                return EditorUtils.GetPrefabsByType<SFGround>(prefabs);
            case CreateTerrainType.Wall:
                return EditorUtils.GetPrefabsByType<SFWall>(prefabs);
            case CreateTerrainType.Ice:
                return EditorUtils.GetPrefabsByType<SFIce>(prefabs);
            case CreateTerrainType.Spike:
                return EditorUtils.GetPrefabsByType<SFSpike>(prefabs);
            case CreateTerrainType.Eletic:
                return EditorUtils.GetPrefabsByType<SFEletric>(prefabs);
            case CreateTerrainType.Sticky:
                return EditorUtils.GetPrefabsByType<SFSticky>(prefabs);
        }

        return null;
    }

}
