﻿public class CommonEnum
{
}

#region map related

public enum SurfaceFeatureType
{
    Ground,
    Wall,
    Ice,
    Spike,
    Eletic,
    Sticky
}

public enum MechanicType
{
    Wind,
    Cannon,
    Teleport,
    StartPoint,
    SavePoint,
    EndPoint,
    RandomDisappear,
    Breakable,
    Trigger,
    Jumpbox,
    Move,
    Rotate
}

public enum ItemType {
    Key
}

#endregion

#region trigger related
public enum TriggerMode
{
    Switch,
    Direction
}

public enum TriggerType {
    LeftRight,
    TopBottom
}

public enum SwitchType {
    On,
    Off
}

public enum DirectionType {
    Left,
    Right
}
#endregion

#region player related
public enum PlayerState
{
    Idle = 0,
    Swing = 1,
    Released = 2
}
#endregion

#region control related
public enum MotionGestureCenterMode
{
    Player = 0,
    Mouse = 1,
    Center = 2,
    Custom = 3
}

public enum MouseControlMode
{
    ClickToSwing = 0,
    HoldToSwing = 1
}
#endregion