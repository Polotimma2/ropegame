﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonFunction : MonoBehaviour
{
    public static CommonFunction call;

    private void Awake()
    {
        if (call == null)
            call = this;
    }

    /// <summary>
    /// Call function after specificed seconds
    /// </summary>
    /// <param name="callFunc">call function.</param>
    /// <param name="waitSecond">delay seconds.</param>
    public void DelayFunc(Action callFunc, float waitSecond)
    {
        StartCoroutine(DelayFuncCoroutine(callFunc, waitSecond));
    }

    private IEnumerator DelayFuncCoroutine(Action callFunc, float waitSecond)
    {
        yield return new WaitForSeconds(waitSecond);
        callFunc();
    }

    public static Vector2 GetRelativePos2D(Vector3 objPos, Vector3 centerPos) {
        return new Vector2(objPos.x - centerPos.x, objPos.y - centerPos.y);
    }

    public static  IEnumerator PlaySpriteAnimation(SpriteRenderer sr,  Sprite[] targetSprites, float switchSpriteTime)
    {
        for (int i = 1; i < targetSprites.Length; i++)
        {
            sr.sprite = targetSprites[i];
            yield return new WaitForSeconds(switchSpriteTime);
        }
    }
}
