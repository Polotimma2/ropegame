﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameSetting
{
    #region player related
    /// <summary>
    /// Player Move Speed.
    /// </summary>
    public const float PLAYER_DEFAULT_SPEED = 3f;
    
    /// <summary>
    /// Gravity of player.
    /// </summary>
    public const float PLAYER_DEFAULT_GRAVITY = 0.85f;

    /// <summary>
    /// Default mass of player.
    /// </summary>
    public const float PLAYER_DEFAULT_MASS = 30f;

    /// <summary>
    /// Add gravity after seconds of flying
    /// </summary>
    public const float PLAYER_ADD_GRAVITY_TIME = 0.5f;

    /// <summary>
    /// Max Gravity of player.
    /// </summary>
    public const float PLAYER_MAX_GRAVITY = 1f;


    #endregion

    #region hook related
    /// <summary>
    /// Forces parameter in add torque formula.
    /// (Torque = radius * force * coefficent)
    /// </summary>
    public const float HOOK_TORQUE_FORCE = 30f;

    /// <summary>
    /// Radius parameter in add torque formula.
    /// (Torque = radius * force * coefficent)
    /// </summary>
    public const float HOOK_TORQUE_RADIUS = 3f;

    /// <summary>
    /// Acceleration of hook. Use to calculate the coefficent of force add to the weight.
    /// Coefficent = Mouse Moved Distance (from last frame) * TORQUE_ACC 
    /// (Torque = radius * force * coefficent)
    /// </summary>
    public const float HOOK_TORQUE_ACC = 2.3f;

    /// <summary>
    /// Force add to the weight after release the rope.
    /// Force = FLY_FORCE * fly direction
    /// </summary>
    public const float HOOK_FLY_FORCE = 80f;

    /// <summary>
    /// Add-on acceleration of hook when there's combo
    /// Coefficent = Mouse Moved Distance (from last frame) * TORQUE_ACC * (1 + COMBO_ADDON_TORQUE * flyCombo)
    /// </summary>
    public const float HOOK_COMBO_ADDON_TORQUE = 0.1f;

    /// <summary>
    /// Bonus of fly force when theres combo.
    /// (NEW_FLY_FORCE = FLY_FORCE * (1 + COMBO_FLY_BONUS * flyCombo);
    /// </summary>
    public const float HOOK_COMBO_FLY_BONUS = 0.1f;

    /// <summary>
    /// maximum accelaration of hook where there's combo
    /// Coefficent = Mouse Moved Distance (from last frame) *TORQUE_ACC *  Math.min((1 + COMBO_ADDON_TORQUE) * flyCombo, comboMaxTorque)
    /// </summary>
    public const float HOOK_COMBO_MAX_FLY_BONUS = 10;

    /// <summary>
    /// combo cool down
    /// </summary>
    public const float HOOK_COMBO_CD = 0.1f;

    #endregion
}
