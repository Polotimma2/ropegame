﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHandler : MonoBehaviour
{
    public delegate void OnPlayerTouchSpikeDelegate();
    public static event OnPlayerTouchSpikeDelegate playerTouchSpikeDelegate;

    public void OnPlayerTouchSpike() {
        playerTouchSpikeDelegate();
    }
}
