﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    // GameObject
    [Header("GameObjects")]

    [Tooltip("The player object.")]
    public PlayerView player; // player object

    [Tooltip("The hook object.")]
    public HookView hook; // hook object

    [Tooltip("The hook UI object.")]
    public GameObject povit; // povit point

    [Header("Setting Option")]

    [Tooltip("Override inspector setting with default setting.")]
    public bool applyDefaultSetting = false;

    [Header("Player Setting")]
    /// <summary>
    /// Player Move Speed.
    /// </summary>
    public float playerSpeed;

    /// <summary>
    /// Gravity of player.
    /// </summary>
    public float playerGravity;

    /// <summary>
    /// Default mass of player.
    /// </summary>
    public float playerMass;

    /// <summary>
    /// Add gravity after seconds of flying
    /// </summary>
    public float playerAddGravityTime;

    /// <summary>
    /// Max Gravity of player.
    /// </summary>
    public float playerMaxGravity;

    [Header("Hook Setting")]
    /// <summary>
    /// Forces parameter in add torque formula.
    /// (Torque = radius * force * coefficent)
    /// </summary>
    public float hookTorqueForce;

    /// <summary>
    /// Radius parameter in add torque formula.
    /// (Torque = radius * force * coefficent)
    /// </summary>
    public float hookTorqueRadius;

    /// <summary>
    /// Acceleration of hook. Use to calculate the coefficent of force add to the weight.
    /// Coefficent = Mouse Moved Distance (from last frame) * TORQUE_ACC 
    /// (Torque = radius * force * coefficent)
    /// </summary>
    public float hookTorqueAcc;

    /// <summary>
    /// Force add to the weight after release the rope.
    /// Force = FLY_FORCE * fly direction
    /// </summary>
    public float hookFlyForce;

    [Header("Combo Setting")]

    /// <summary>
    /// Add-on acceleration of hook when there's combo
    /// Coefficent = Mouse Moved Distance (from last frame) * TORQUE_ACC * (1 + COMBO_ADDON_TORQUE) * flyCombo
    /// </summary>
    public float comboAddOnTorque;

    /// <summary>
    /// Bonus of fly force when theres combo.
    /// (NEW_FLY_FORCE = FLY_FORCE * (1 + COMBO_FLY_BONUS) * flyCombo;
    /// </summary>
    public float comboFlyBonus;

    /// <summary>
    /// maximum accelaration of hook where there's combo
    /// Coefficent = Mouse Moved Distance (from last frame) *TORQUE_ACC *  Math.min((1 + COMBO_ADDON_TORQUE) * flyCombo, comboMaxTorque)
    /// </summary>
    public float comboMaxFlyBonus;

    /// <summary>
    /// combo cool down
    /// </summary>
    public float comboCD;

    public MotionGestureCenterMode centerMode { get { return _centerMode; } }
    private MotionGestureCenterMode _centerMode = MotionGestureCenterMode.Player;

    public Vector3 customCenterPos = new Vector3(9,-2);

    public MouseControlMode mouseCtrlMode { get { return _mouseCtrlMode; } }
    private MouseControlMode _mouseCtrlMode = MouseControlMode.ClickToSwing;

    

    public PlayerState state { get { return _state; } }
    private PlayerState _state = PlayerState.Idle; // player state


    private void Awake()
    {
        if (instance == null)
            instance = this;

        //FindComponent, assume there is only one gameobject attached the script on the scene
        player = FindObjectOfType<PlayerView>();
        player.ApplyPlayerSetting();

        hook = FindObjectOfType<HookView>();
        povit = transform.Find("Pivot").gameObject;

        _mouseCtrlMode = MouseControlMode.HoldToSwing;

        if (applyDefaultSetting)
        {
            playerSpeed = GameSetting.PLAYER_DEFAULT_SPEED;
            playerGravity = GameSetting.PLAYER_DEFAULT_GRAVITY;
            playerMass = GameSetting.PLAYER_DEFAULT_MASS;
            playerAddGravityTime = GameSetting.PLAYER_ADD_GRAVITY_TIME;
            playerMaxGravity = GameSetting.PLAYER_MAX_GRAVITY;

            hookTorqueForce = GameSetting.HOOK_TORQUE_FORCE;
            hookTorqueRadius = GameSetting.HOOK_TORQUE_RADIUS;
            hookTorqueAcc = GameSetting.HOOK_TORQUE_ACC;
            hookFlyForce = GameSetting.HOOK_FLY_FORCE;
            comboAddOnTorque = GameSetting.HOOK_COMBO_ADDON_TORQUE;
            comboFlyBonus = GameSetting.HOOK_COMBO_FLY_BONUS;

            comboMaxFlyBonus = GameSetting.HOOK_COMBO_MAX_FLY_BONUS;
            comboCD = GameSetting.HOOK_COMBO_CD;
        }
    }

    private void Start()
    {
        if (UIGameManager.instance != null)
        UIGameManager.instance.Init();

        //Start State
        player.SetState(PlayerState.Idle);
        hook.SetState(PlayerState.Idle);
    }

    // Update is called once per frame
    void Update()
    {
        // Switch Center Mode
        if (Input.GetKeyDown(KeyCode.Tab)) {
            if (_centerMode == MotionGestureCenterMode.Player)
                _centerMode = MotionGestureCenterMode.Mouse;
            else if (_centerMode == MotionGestureCenterMode.Mouse)
                _centerMode = MotionGestureCenterMode.Center;
            else if (_centerMode == MotionGestureCenterMode.Center)
                _centerMode = MotionGestureCenterMode.Custom;
            else if (_centerMode == MotionGestureCenterMode.Custom)
                _centerMode = MotionGestureCenterMode.Player;

        }

        /*
        // Switch Mouse Swing Mode
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (_mouseCtrlMode == MouseControlMode.ClickToSwing)
            {
                _mouseCtrlMode = MouseControlMode.HoldToSwing;
                SetState(PlayerState.Idle);
            }
            else
            {
                _mouseCtrlMode = MouseControlMode.ClickToSwing;
                SetState(PlayerState.Idle);
            }
        }

        if (_mouseCtrlMode == MouseControlMode.ClickToSwing)
        {
            // Status Update
            if (Input.GetMouseButtonDown(0))
            {
                switch (_state)
                {
                    case PlayerState.Idle:
                        SetState(PlayerState.Swing);
                        break;
                    case PlayerState.Swing:
                        SetState(PlayerState.Released);
                        break;
                    case PlayerState.Released:
                        if (player.isFalling == false)
                            SetState(PlayerState.Swing);
                        break;
                }
            }
        }
        */

        if (_mouseCtrlMode == MouseControlMode.HoldToSwing) {
            // Status Update
            if (Input.GetMouseButtonDown(0))
            {
                if (player.isReadyToCombo)
                {
                    switch (_state)
                    {
                        case PlayerState.Idle:
                            SetState(PlayerState.Swing);
                            break;
                        case PlayerState.Swing:
                            //SetState(PlayerState.Released);
                            break;
                        case PlayerState.Released:
                            if (player.isFalling == false)
                                SetState(PlayerState.Swing);
                            break;
                    }
                } 
            }

            if (Input.GetMouseButtonUp(0))
            {
                // check is the combo cd passed
                if (_state == PlayerState.Swing)
                {
                    SetState(PlayerState.Released);
                }
            }
        }

        //click R to reset
        if (Input.GetKeyDown(KeyCode.R))
        {
            SetState(PlayerState.Idle);
            player.Reset();
        }
    }

    public void SetState(PlayerState state) {

        switch (state)
        {
            case PlayerState.Idle:
                player.SetState(PlayerState.Idle);
                hook.SetState(PlayerState.Idle);
                break;
            case PlayerState.Swing:
                player.SetState(PlayerState.Swing);
                hook.SetState(PlayerState.Swing);

                break;
            case PlayerState.Released:

                // Set player state
                player.SetState(PlayerState.Released);

                // Open collision of weight
                hook.SetState(PlayerState.Released);

                if(player.isFalling == false)
                    hook.AddForceAtWeightRelativeTo(player.transform.position);

                break;
        }

        _state = state;

    }

    #region Call by surfacefeature
    public void KillPlayer()
    {
        Debug.Log("KillPlayer");
        player.Reset();
        SetState(PlayerState.Idle);
    }

    public void EleticPlayer()
    {
        Debug.Log("EleticPlayer");
        SetState(PlayerState.Idle);
        player.SetSpeedToZero();
    }
    #endregion

    #region Called by mechanic
    public void EndGame() {
        Debug.Log("EndGame");
    }

    public void SetSavePoint(Vector3 savePoint) {
        player.SetNextStartPoint(savePoint);
    }
    #endregion
}
