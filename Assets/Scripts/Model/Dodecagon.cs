﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dodecagon : MonoBehaviour
{
    public bool ground1, ground2, ground3, ground4, ground5, ground6, ground7, ground8, ground9, ground10, ground11, ground12;
    // Start is called before the first frame update
    public float rotaSpeed;
    public bool rotaToR;



    // Update is called once per frame
    void Start()
    {
        SetGroundEnable();
    }

    private void Update()
    {
        Rotation360();
    }
    private void SetGroundEnable()
    {
        if(ground1)
         transform.GetChild(0).gameObject.SetActive(true);
        else
            transform.GetChild(0).gameObject.SetActive(false);

        if (ground2)
            transform.GetChild(1).gameObject.SetActive(true);
        else
            transform.GetChild(1).gameObject.SetActive(false);

        if (ground3)
            transform.GetChild(2).gameObject.SetActive(true);
        else
            transform.GetChild(2).gameObject.SetActive(false);

        if (ground4)
            transform.GetChild(3).gameObject.SetActive(true);
        else
            transform.GetChild(3).gameObject.SetActive(false);

        if (ground5)
            transform.GetChild(4).gameObject.SetActive(true);
        else
            transform.GetChild(4).gameObject.SetActive(false);

        if (ground6)
            transform.GetChild(5).gameObject.SetActive(true);
        else
            transform.GetChild(5).gameObject.SetActive(false);

        if (ground7)
            transform.GetChild(6).gameObject.SetActive(true);
        else
            transform.GetChild(6).gameObject.SetActive(false);

        if (ground8)
            transform.GetChild(7).gameObject.SetActive(true);
        else
            transform.GetChild(7).gameObject.SetActive(false);

        if (ground9)
            transform.GetChild(8).gameObject.SetActive(true);
        else
            transform.GetChild(8).gameObject.SetActive(false);

        if (ground10)
            transform.GetChild(9).gameObject.SetActive(true);
        else
            transform.GetChild(9).gameObject.SetActive(false);

        if (ground11)
            transform.GetChild(10).gameObject.SetActive(true);
        else
            transform.GetChild(10).gameObject.SetActive(false);

        if (ground12)
            transform.GetChild(11).gameObject.SetActive(true);
        else
            transform.GetChild(11).gameObject.SetActive(false);

    }

    private void Rotation360()
    {
        if(rotaToR)
        transform.Rotate(0, 0, rotaSpeed * Time.deltaTime);
        else
            transform.Rotate(0, 0, -rotaSpeed * Time.deltaTime);
    }
}

