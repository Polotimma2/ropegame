﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable] 

public class Mechanic : MonoBehaviour
{
    public MechanicType type { get { return _type; } }
    protected MechanicType _type;

    [Header("Trigger Setting")]
    public SwitchType leftSwitch = SwitchType.On;
    public DirectionType leftDirection = DirectionType.Left;

    public virtual void OnTriggerLeft(TriggerMode mode) { }

    public virtual void OnTriggerRight(TriggerMode mode) { }
}
