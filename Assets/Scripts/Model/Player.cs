﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    public float speed { get { return _speed; } set { _speed = value; } }
    private float _speed;

    public float gravity { get { return _gravity; } set { _gravity = value; } }
    private float _gravity;

    public float mass { get { return _mass; } set { _mass = value; } }
    private float _mass;

    public Player() {
        _speed = GameSetting.PLAYER_DEFAULT_SPEED;
        _gravity = GameSetting.PLAYER_DEFAULT_GRAVITY;
        _mass = GameSetting.PLAYER_DEFAULT_MASS;
    }

    public Player(float newSpeed, float newGravity, float newMass)
    {
        _speed = newSpeed;
        _gravity = newGravity;
        _mass = newMass;
    }
}
