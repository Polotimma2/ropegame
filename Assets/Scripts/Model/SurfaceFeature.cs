﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceFeature : MonoBehaviour
{
    public Collider2D collider { get { return _collider; } }
    protected Collider2D _collider;

    public SurfaceFeatureType feature { get { return _feature; } }
    protected SurfaceFeatureType _feature = SurfaceFeatureType.Ground; // default

    [Header("Trigger Setting")]
    public SwitchType leftSwitch = SwitchType.On;

    private void Awake()
    {
        _collider = GetComponent<Collider2D>();

        AwakeInit();
    }

    /// <summary>
    /// call after Awake()
    /// </summary>
    public virtual void AwakeInit() { }

    /// <summary>
    /// called by trigger mechanic.
    /// </summary>
    /// <param name="mode"></param>
    public virtual void OnTriggerLeft(TriggerMode mode) { }

    /// <summary>
    /// called by trigger mechanic.
    /// </summary>
    /// <param name="mode"></param>
    public virtual void OnTriggerRight(TriggerMode mode) { }
}
