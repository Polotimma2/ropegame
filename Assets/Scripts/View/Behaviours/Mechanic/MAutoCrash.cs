﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAutoCrash : Mechanic
{
    [Tooltip("Trigger time to crash")]
    public float triggerTime;
    [Tooltip("respawnTime -1 : never respawn")]
    public float respawnTime;

    public Sprite[] crashSpriteList;
    public float switchSpriteTime;

    private bool isDisappear = false;
    private SpriteRenderer sr;
    // Start is called before the first frame update

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player" && collision.transform.position.y > transform.position.y)
        {
            StartCoroutine(Disappear());
        }
    }

    private IEnumerator Disappear()
    {
        isDisappear = true;
        yield return new WaitForSeconds(triggerTime);

        GetComponent<Collider2D>().enabled = false;
        if (respawnTime > 0)
        {
            StartCoroutine(CommonFunction.PlaySpriteAnimation(sr, crashSpriteList, switchSpriteTime));
            yield return new WaitForSeconds(respawnTime);
            GetComponent<Collider2D>().enabled = true;
            sr.sprite = crashSpriteList[0];
            isDisappear = false;
        }
        else
        {
            yield return StartCoroutine(CommonFunction.PlaySpriteAnimation(sr, crashSpriteList, switchSpriteTime));
            Destroy(gameObject);
        }
    }
}
