﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MBullet : MonoBehaviour
{
    public float autoDestryTime = 5.0f;
    public bool kill;
    private float time;
    // Start is called before the first frame update
    void Start()
    {
        time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time > autoDestryTime)
            AutoDestroy();
    }

    void AutoDestroy()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if (kill)
                GameManager.instance.KillPlayer();
            else
                GameManager.instance.EleticPlayer();

            AutoDestroy();
        }
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag  == "Wall")
        {
            AutoDestroy();
        }
    }
}
