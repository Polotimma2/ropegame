﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCannon : Mechanic
{
    public GameObject bullet;

    public Vector3 addForce;
    public Vector3 secondAddForce;

    public float looptime;
    public float cooldowntime;
    public bool canFire;

    private float time;
    private float currlooptime;

    public bool isTriggerLeft;
    // Start is called before the first frame update
    private void Awake()
    {
        if (bullet == null)
            bullet = transform.Find("Bullet").gameObject;
    }
    void Start()
    {
        currlooptime = 0;
        time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(canFire)
        {
            time += Time.deltaTime;
            if(time > cooldowntime &&( currlooptime < looptime || looptime == -1))
            {
                Fire();
                time = 0;
            }
        }
    }

    void Fire()
    {
        GameObject newAmmno = Instantiate(bullet);
        newAmmno.transform.position = bullet.transform.position;
        newAmmno.SetActive(true);
        if(isTriggerLeft)
            newAmmno.GetComponent<Rigidbody2D>().AddForce(addForce, ForceMode2D.Force);
        else
            newAmmno.GetComponent<Rigidbody2D>().AddForce(secondAddForce, ForceMode2D.Force);

    }

    public override void OnTriggerLeft(TriggerMode mode)
    {
        if (mode == TriggerMode.Switch)
            canFire = leftSwitch == SwitchType.On;
        else if (mode == TriggerMode.Direction)
        {
            isTriggerLeft = true;
        }
    }

    public override void OnTriggerRight(TriggerMode mode)
    {
        if (mode == TriggerMode.Switch)
            canFire = !(leftSwitch == SwitchType.On);
        else if (mode == TriggerMode.Direction)
        {
            isTriggerLeft = false;
        }
    }
}
