﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MDoor : Mechanic
{
    public Item requireItem;
    public bool isOpen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(collision.transform.GetComponent<PlayerView>().CheckHaveRequreItem(requireItem))
            {
                isOpen = true;
                OpenTrigger();
            }
        }
    }

    public override void OnTriggerLeft(TriggerMode mode)
    {
        if (mode == TriggerMode.Switch)
            isOpen = leftSwitch == SwitchType.On;
          
        OpenTrigger();
       }

    public override void OnTriggerRight(TriggerMode mode)
    {
        if (mode == TriggerMode.Switch)
            isOpen = !(leftSwitch == SwitchType.On);

        OpenTrigger();
    }

    public void OpenTrigger()
    {
        GetComponent<Collider2D>().enabled = !isOpen;
    }
}
