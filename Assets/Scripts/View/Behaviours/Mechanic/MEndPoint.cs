﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MEndPoint : Mechanic
{
    private void Awake()
    {
        _type = MechanicType.EndPoint;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            GameManager.instance.EndGame();
        }
    }

}
