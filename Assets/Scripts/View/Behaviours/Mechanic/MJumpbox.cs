﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MJumpbox : Mechanic
{
    public Vector3 addForce;
    public Sprite[] triggerSprites;
    public float switchSpriteTime;
    private SpriteRenderer sr;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.transform.GetComponent<Rigidbody2D>().mass = 100;
            collision.transform.GetComponent<Rigidbody2D>().AddForce(addForce);
            StartCoroutine(CommonFunction.PlaySpriteAnimation(sr, triggerSprites, switchSpriteTime));
        }
    }
}
