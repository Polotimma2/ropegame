﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSavePoint : Mechanic
{
    public Vector3 savePos { get { return _savePos; } }
    private Vector3 _savePos;

    private void Awake()
    {
        _type = MechanicType.SavePoint;
        _savePos = transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {
        gameObject.tag = "SavePoint";
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            GameManager.instance.SetSavePoint(_savePos);
        }
    }
}
