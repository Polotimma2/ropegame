﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MTeleport : Mechanic
{
    public MTeleport teleportTo;

    private bool isClose = false;

    private void Awake()
    {
        _type = MechanicType.Teleport;
    }

    public void SetClose() {
        isClose = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            if (!isClose && teleportTo != null)
            {
                teleportTo.SetClose();
                GameManager.instance.player.transform.position = teleportTo.transform.position;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (isClose)
            {
                isClose = false;
            }
        }
    }

}
