﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MTrigger : Mechanic
{
    public delegate void OnLeftDelegate<MechanicTriggerMode>(MechanicTriggerMode mode);
    public  OnLeftDelegate<TriggerMode> leftDelegate;

    public delegate void OnRightDelegate<MechanicTriggerMode>(MechanicTriggerMode mode);
    public  OnRightDelegate<TriggerMode> rightDelegate;

    public TriggerType type = TriggerType.LeftRight;

    public SurfaceFeature targetSF;
    public Mechanic targetM;
    public TriggerMode mode = TriggerMode.Switch;

    private MTriggerComponent colLeft;
    private MTriggerComponent colRight;
    private MTriggerComponent colTop;
    private MTriggerComponent colBottom;
    private MTriggerComponent enterFrom;

    private SpriteRenderer render;
    public Sprite spriteLeft;
    public Sprite spriteRight;

    public bool defaultSetting = true;

    public bool isLeft { get { return _isLeft; } }
    private bool _isLeft;

    private bool allowTrigger = false;
    private float triggerCD = 0.5f;
    private float currTriggerCD = 0f;

  

    private void Awake()
    {
        render = GetComponent<SpriteRenderer>();
        colLeft = transform.Find("Left").GetComponent<MTriggerComponent>();
        colRight = transform.Find("Right").GetComponent<MTriggerComponent>();
        colTop = transform.Find("Top").GetComponent<MTriggerComponent>();
        colBottom = transform.Find("Bottom").GetComponent<MTriggerComponent>();

        _type = MechanicType.Trigger;
        _isLeft = defaultSetting;
        render.sprite = spriteRight;

        //Set Delegate
        if (targetSF != null)
        {
            leftDelegate += targetSF.OnTriggerLeft;
            rightDelegate += targetSF.OnTriggerRight;
        }

        if (targetM != null)
        {
            leftDelegate += targetM.OnTriggerLeft;
            rightDelegate += targetM.OnTriggerRight;
        }

        colLeft.SetDelegate(SetEnterDirection);
        colRight.SetDelegate(SetEnterDirection);
        colTop.SetDelegate(SetEnterDirection);
        colBottom.SetDelegate(SetEnterDirection);
    }

    public void SetEnterDirection(Collider2D collision, MTriggerComponent component)
    {
        if (type == TriggerType.LeftRight) {
            if (component == colLeft || component == colRight)
                enterFrom = component;
        } else if(type == TriggerType.TopBottom)
        {
            if (component == colTop || component == colBottom)
                enterFrom = component;
        }
    }

    private void FixedUpdate()
    {
        if (!allowTrigger) {
            currTriggerCD += Time.fixedDeltaTime;
            if (currTriggerCD >= triggerCD) {
                allowTrigger = true;
                currTriggerCD = 0;
            }
        }
    }

    private void Start()
    {
        if (mode == TriggerMode.Switch)
        {
            if (_isLeft)
                OnTriggerLeft();
            else
                OnTriggerRight();
        }
        else if (mode == TriggerMode.Direction) {
            if (_isLeft)
            {
                _isLeft = true;
                render.sprite = spriteLeft;
            }
            else
            {
                _isLeft = false;
                render.sprite = spriteRight;
                OnTriggerRight();
            }
        }

    }

    public void OnTriggerLeft() {
        _isLeft = true;
        render.sprite = spriteLeft;
        leftDelegate(mode);

    }

    public void OnTriggerRight() {
        _isLeft = false;
        render.sprite = spriteRight;
        rightDelegate(mode);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (allowTrigger)
        {
            if (collision.gameObject.tag == "Hook")
            {
                if (isLeft)
                {
                    // trigger only if player come from left or top
                    if ((type == TriggerType.LeftRight && enterFrom == colLeft) || (type == TriggerType.TopBottom && enterFrom == colTop))
                        OnTriggerRight();
                }
                else
                {
                    // trigger only if player come from right or bottom
                    if ((type == TriggerType.LeftRight && enterFrom == colRight) || (type == TriggerType.TopBottom && enterFrom == colBottom))
                        OnTriggerLeft();
                }

                allowTrigger = false;
            }
        }
    }
}
