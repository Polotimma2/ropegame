﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MTriggerComponent : MonoBehaviour
{
    public delegate void OnTriggerEnterDelegate<MTriggerComponent>(Collider2D collision, MTriggerComponent component);
    public  OnTriggerEnterDelegate<MTriggerComponent> onEnterTriggerDelegate;

    public void SetDelegate(OnTriggerEnterDelegate<MTriggerComponent> callback) {
        onEnterTriggerDelegate += callback;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(onEnterTriggerDelegate != null)
            onEnterTriggerDelegate(collision, this);
    }

    public delegate void OnCollisionEnterDelegate<MTriggerComponent>(Collision2D collision, MTriggerComponent component);
    public OnCollisionEnterDelegate<MTriggerComponent> onEnterCollisionDelegate;

    public void SetDelegate(OnCollisionEnterDelegate<MTriggerComponent> callback)
    {
        onEnterCollisionDelegate += callback;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(onEnterCollisionDelegate != null)
            onEnterCollisionDelegate(collision, this);
    }
}
