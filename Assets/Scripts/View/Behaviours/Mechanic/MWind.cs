﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MWind : Mechanic
{
    private AreaEffector2D myEffector;
    public float force20 = 850;
    public float force2000 = 30050;

    private void Awake()
    {
        _type = MechanicType.Wind;
        myEffector = GetComponent<AreaEffector2D>();
    }

    private void FixedUpdate()
    {
        if(GameManager.instance.player.myRb.mass == GameManager.instance.playerMass) {
            myEffector.forceMagnitude = force20;
        } else
        {
            myEffector.forceMagnitude = force2000;
        }
    }
}
