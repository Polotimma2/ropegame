﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePath : Mechanic
{
    public List<Vector2> posList;
    public float moveSpeedPos,moveSpeedNega;
    public bool isAutoStart;
    private bool isMoving;

    private int currentIndex;
    private Vector2 targetPos;

    public delegate void OnArriveCheckPoint<MovePath>(MovePath path, int index);
    public  OnArriveCheckPoint<MovePath> onArriveCheckPointDelegate;

    private bool isBackward = false;

    // Start is called before the first frame update
    void Start()
    {
        currentIndex = 0;
        targetPos = posList[0];
    }

    public void SetDelegate(OnArriveCheckPoint<MovePath> callback){
        onArriveCheckPointDelegate += callback;
    }

    public void ChangeDirection()
    {
        isBackward = !isBackward;

        if (isBackward)
        {
            currentIndex--;
            if (currentIndex < 0)
                currentIndex = posList.Count - 1;
        }
        else
        {
            currentIndex++;
            if (currentIndex >= posList.Count)
                currentIndex = 0;
        }

        targetPos = posList[currentIndex];
    }

    // Update is called once per frame
    void Update()
    {
        if(isAutoStart || isMoving)
        {
            if (isBackward)
            {
                float step = moveSpeedPos * Time.deltaTime; // calculate distance to move
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPos, step);
            } else
            {
                float step = -moveSpeedNega * Time.deltaTime; // calculate distance to move
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPos, step);
            }

            // Check if the position of the cube and sphere are approximately equal.
            if (Vector3.Distance(transform.localPosition, targetPos) < 0.001f)
            {
                // Swap the position of the cylinder.
                if (isBackward)
                {
                    currentIndex--;
                    if (currentIndex < 0)
                        currentIndex = posList.Count - 1;
                } else
                {
                    currentIndex++;
                    if (currentIndex >= posList.Count)
                        currentIndex = 0;
                }

                targetPos = posList[currentIndex];
                onArriveCheckPointDelegate(this, currentIndex);

            }
        }
    }

    public void StopMove()
    {
        isMoving = false;
        isAutoStart = false;
    }

    public void OnAnimatorMove()
    {
        isMoving = true;
    }
}
