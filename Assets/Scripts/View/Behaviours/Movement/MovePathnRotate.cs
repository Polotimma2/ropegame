﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePathnRotate : MonoBehaviour
{
    public DirectionType defaultDirection = DirectionType.Right;
    public List<Vector2> defaultMovePath;
    public List<float> defaultAngle;

    public float moveSpeed;
    private DirectionType _currDirection;

    private void Awake()
    {
        _currDirection = defaultDirection;

        foreach (Transform ch in transform)
        {
            MovePath script = ch.gameObject.GetComponent<MovePath>();
            if (script == null)
                script = ch.gameObject.AddComponent<MovePath>() as MovePath;

            script.posList = defaultMovePath;

            script.moveSpeedPos = moveSpeed;
            script.isAutoStart = true;
            script.SetDelegate(OnArriveCheckPoint);
        }
    }

    public void ChangeDirection()
    {
        if (_currDirection == DirectionType.Left)
            _currDirection = DirectionType.Right;
        else
            _currDirection = DirectionType.Left;

        foreach (Transform ch in transform)
        {
            MovePath script = ch.gameObject.GetComponent<MovePath>();
            if (script == null)
                script = ch.gameObject.AddComponent<MovePath>() as MovePath;

            script.ChangeDirection();
        }
    }

    protected void OnArriveCheckPoint(MovePath path, int index) {

        if (_currDirection == DirectionType.Right)
            path.transform.rotation = Quaternion.Euler(0, 0, defaultAngle[index]);
        else if (_currDirection == DirectionType.Left) {
            if(index == defaultAngle.Count - 1)
                path.transform.rotation = Quaternion.Euler(0, 0, defaultAngle[0]);
            else
                path.transform.rotation = Quaternion.Euler(0, 0, defaultAngle[index + 1]);
        }
    }

}
