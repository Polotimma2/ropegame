﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MoveRotate : Mechanic
{
    public Vector3 direction;
    public float angle;

    public Vector3 secondDirection;
    public float secondAngle;

    public bool canRotate = true;
    public  bool isTriggerLeft = true;
    // Update is called once per frame

    void Update()
    {
        if (canRotate)
        {
            if(isTriggerLeft)
                transform.Rotate(direction, angle);
            else
                transform.Rotate(secondDirection, secondAngle);
        }
    }

    public override void OnTriggerLeft(TriggerMode mode)
    {
        if (mode == TriggerMode.Switch)
            canRotate = leftSwitch == SwitchType.On;
        else if (mode == TriggerMode.Direction)
        {
            isTriggerLeft = true;
        }
    }

    public override void OnTriggerRight(TriggerMode mode)
    {
        if (mode == TriggerMode.Switch)
            canRotate = !(leftSwitch == SwitchType.On);
        else if (mode == TriggerMode.Direction)
        {
            isTriggerLeft = false;
        }
    }
}
