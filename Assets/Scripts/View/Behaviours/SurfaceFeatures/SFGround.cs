﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFGround : SurfaceFeature
{
    /// <summary>
    /// Called after Awake()
    /// </summary>
    public override void AwakeInit() {
        _feature = SurfaceFeatureType.Ground;
    }

    private void Start()
    {
        gameObject.tag = "Ground";
    }
}
