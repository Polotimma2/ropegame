﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFIce : SurfaceFeature
{
    /// <summary>
    /// Called after Awake()
    /// </summary>
    public override void AwakeInit()
    {
        _feature = SurfaceFeatureType.Ice;
    }

    private void Start()
    {
        gameObject.tag = "Ground";
    }
}
