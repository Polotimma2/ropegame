﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFSpike : SurfaceFeature
{
    // only use for moving Spike
    private MovePathnRotate moveScript;

    public override void AwakeInit()
    {
        _feature = SurfaceFeatureType.Spike;
        moveScript = GetComponent<MovePathnRotate>();
    }

    private void Start()
    {
        foreach (Transform ch in transform)
        {
            MTriggerComponent trigger = ch.GetComponent<MTriggerComponent>();
            if (trigger != null)
            {
                trigger.SetDelegate(OnComponentCollisionEnter2D);
            }
        }
    }

    public void OnComponentCollisionEnter2D(Collision2D collision, MTriggerComponent component)
    {
        OnCollisionEnter2D(collision);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.instance.KillPlayer();
        }

        if (moveScript != null)
        {
            if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Wall")
            {
                moveScript.ChangeDirection();
            }
        }
    }

    public override void OnTriggerLeft(TriggerMode mode)
    {
        if (mode == TriggerMode.Switch)
            gameObject.SetActive(leftSwitch == SwitchType.On);
        else if (mode == TriggerMode.Direction && moveScript != null)
            moveScript.ChangeDirection();
    }

    public override void OnTriggerRight(TriggerMode mode)
    {
        if (mode == TriggerMode.Switch)
            gameObject.SetActive(leftSwitch != SwitchType.On);
        else if (mode == TriggerMode.Direction && moveScript != null)
            moveScript.ChangeDirection();
    }
}
