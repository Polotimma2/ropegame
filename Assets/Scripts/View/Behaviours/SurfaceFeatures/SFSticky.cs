﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFSticky : SurfaceFeature
{
    public GameObject parentObj;
    public GameObject childrenObj;
    public override void AwakeInit()
    {
        _feature = SurfaceFeatureType.Sticky;
    }

    private void Start()
    {
        if(parentObj != null && childrenObj != null)
        {
            childrenObj.transform.SetParent(parentObj.transform);
            transform.SetParent(parentObj.transform);
        }
    }
}
