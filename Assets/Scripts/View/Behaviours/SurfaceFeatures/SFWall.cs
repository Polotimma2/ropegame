﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFWall : SurfaceFeature
{
    /// <summary>
    /// Called after Awake()
    /// </summary>
    public override void AwakeInit()
    {
        _feature = SurfaceFeatureType.Wall;
    }

    private void Start()
    {
        gameObject.tag = "Wall";
    }


}
