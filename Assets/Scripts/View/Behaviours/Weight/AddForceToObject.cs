﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddForceToObject : MonoBehaviour
{
    public InputField inputAcc;

    public Rigidbody2D rb;
    public Rigidbody2D hook;
    public HingeJoint2D[] nodes;
    public GameObject center;

    //Torque = force * radius * radianFactor * radianDiff
    public float force = 20; // force
    public float radius = 2; // distance
    public float acceleration = 1f; // factor of radianDiff when calculate the cofficient of force
    private float radianDiff = 0; // radian difference of mouse position relative to the center point each frame

    private Vector3 centerPos = Vector3.zero;  // center point of clockwise movement
    private Vector3 currPos = Vector3.zero;  // current mouse position relative to the povit
    private Vector3 lastPos = Vector3.zero; // mouse position relative to the povit in the last frame;

    private float lastRandian = 0, currRadian = 0; // radian of last and current mouse position relative to the center point
    private float hookTan2 = 0f; // radian between hook to rb
    private float tanDiff = 0f; // tan difference between hook to rb and mouse position to povit
    private float circularDisDiff = 0f; // circular distance of mopuse position between 2 frame

    private int lastGuestureMovement = 0; // 1 - clockwise, 2 - anticlockwise

    private float mouseStopCD = 0f;
    private float mouseStopTime = 0.3f;
    public bool isMoving = false;

    public Vector3 mousePos;
    public Vector3 relativePos;

    // Start is called before the first frame update
    void Start()
    {
        if (rb == null)
            rb = GetComponent<Rigidbody2D>();

        if (inputAcc != null)
        {
            inputAcc.text = acceleration.ToString();
            inputAcc.onValueChanged.AddListener(delegate { acceleration = float.Parse(inputAcc.text); });
        }
        mouseStopCD = mouseStopTime;
    }

    private void Update()
    {
        // set povit position
        if (centerPos != Vector3.zero)
        {
            center.transform.position = new Vector3(centerPos.x, centerPos.y, 0);

        }
    }

    public void ResetNodesRotation()
    {
        transform.localRotation = Quaternion.Euler(0, 0, 90);
        int i = 0;
        foreach (HingeJoint2D x in nodes)
        {
            x.transform.localRotation = Quaternion.Euler(0, 0, 0);
            x.transform.localPosition = new Vector3(0, -0.03f - i * 0.3f, 0);
            i++;
        }
    }

    public void StartAddTorque()
    {
        centerPos = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0);
    }

    public void StartAddTorque(Vector3 pos)
    {
        centerPos = pos;
    }

    public void StopAddTorque()
    {
        centerPos = Vector3.zero;
        lastPos = Vector3.zero;

        foreach (HingeJoint2D x in nodes)
        {
            x.useLimits = true;
        }

        rb.angularVelocity = 0;
        isMoving = false;
    }

    public void SetSpeedToZero()
    {
        rb.angularVelocity = 0;
        rb.velocity = Vector2.zero;
        isMoving = false;
    }

    public void SetHingleArray(GameObject[] objlist)
    {
        Array.Clear(nodes, 0, nodes.Length);
        nodes = new HingeJoint2D[objlist.Length - 1];
        for (int i = 0; i < objlist.Length - 1; i++)
        {
            nodes[i] = objlist[i].GetComponent<HingeJoint2D>();
        }
    }

    private void FixedUpdate()
    {
        //Read Mouse Movement: clockwise or anticlockwise
        if (centerPos != Vector3.zero)
        {
            ApplyForce();
        }
    }

    void ApplyTorque(float coefficent)
    {
        Debug.Log("ApplyTorque");
        isMoving = true;
        foreach (HingeJoint2D x in nodes)
        {
            x.useLimits = true;
        }

        rb.AddTorque(force * radius * coefficent);
        //rb.AddRelativeForce(currPos * force * radius * coefficent * 1000);
    }

    void ApplyForce()
    {
        //Calcuate force direction
        mousePos = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0);

        relativePos = new Vector3(mousePos.x - centerPos.x, mousePos.y - centerPos.y, 0);

        //Vector3 direction = new Vector3(Mathf.Min(relativePos.x, directionforce), Mathf.Min(relativePos.y, directionforce));
        Vector3 direction = new Vector3(relativePos.x * force, relativePos.y * force);
        hook.AddForce(direction);
    }

    float RadianRelativeToCenter(Vector3 objPos, Vector3 centerPos)
    {

        Vector3 relativePos = new Vector3(objPos.x - centerPos.x, objPos.y - centerPos.y, 0);
        float radian = 0f;

        //4 Case: (x,y) | (x,-y) | (-x,-y) | (-x,y)
        if (relativePos.x > 0 && relativePos.y > 0)
            radian = Mathf.Atan2(relativePos.x, relativePos.y);
        else if (relativePos.x > 0 && relativePos.y < 0)
            radian = 90 * Mathf.Deg2Rad + Mathf.Atan2(-relativePos.y, relativePos.x);
        else if (relativePos.x < 0 && relativePos.y < 0)
            radian = 180 * Mathf.Deg2Rad + Mathf.Atan2(-relativePos.x, -relativePos.y);
        else if (relativePos.x < 0 && relativePos.y > 0)
            radian = 270f * Mathf.Deg2Rad + Mathf.Atan2(relativePos.y, -relativePos.x);

        return radian;
    }

    Vector3 PositionRelativeToCenter(Vector3 objPos, Vector3 centerPos)
    {
        return new Vector3(objPos.x - centerPos.x, objPos.y - centerPos.y, 0);
    }
}
