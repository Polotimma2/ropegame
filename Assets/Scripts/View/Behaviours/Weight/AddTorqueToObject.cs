﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddTorqueToObject : MonoBehaviour
{
    public InputField inputAcc;

    public Rigidbody2D rb;
    public Rigidbody2D hook;
    public HingeJoint2D[] nodes;
    public GameObject center;

    //Torque = force * radius * radianFactor * radianDiff
    public float force = 20; // force
    public float directionforce = 20; // force
    public float radius = 2; // distance
    public float acceleration = 1f; // factor of radianDiff when calculate the cofficient of force
    private float radianDiff = 0; // radian difference of mouse position relative to the center point each frame

    private Vector3 centerPos = Vector3.zero;  // center point of clockwise movement
    private Vector3 currPos = Vector3.zero;  // current mouse position relative to the povit
    private Vector3 lastPos = Vector3.zero; // mouse position relative to the povit in the last frame;

    private float lastRandian = 0, currRadian = 0; // radian of last and current mouse position relative to the center point
    private float hookTan2 = 0f; // radian between hook to rb
    private float tanDiff = 0f; // tan difference between hook to rb and mouse position to povit
    private float circularDisDiff = 0f; // circular distance of mopuse position between 2 frame

    private int lastGuestureMovement = 0; // 1 - clockwise, 2 - anticlockwise

    private float mouseStopCD = 0f;
    private float mouseStopTime = 0.3f;
    public bool isMoving = false;

    // Start is called before the first frame update
    void Start()
    {
        if (rb == null)
            rb = GetComponent<Rigidbody2D>();

        if (inputAcc != null)
        {
            inputAcc.text = directionforce.ToString();
            inputAcc.onValueChanged.AddListener(delegate { directionforce = float.Parse(inputAcc.text); });
        }
        mouseStopCD = mouseStopTime;
    }

    private void Update()
    {
        // set povit position
        if (centerPos != Vector3.zero)
        {
            center.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(centerPos).x, Camera.main.ScreenToWorldPoint(centerPos).y, 0);
        }
    }

    public void ResetNodesRotation() {
        transform.localRotation = Quaternion.Euler(0, 0, 90);
        int i = 0;
        foreach (HingeJoint2D x in nodes)
        {
            x.transform.localRotation = Quaternion.Euler(0, 0, 0);
            x.transform.localPosition = new Vector3(0, -0.03f- i*0.3f, 0);
            i++;
        }
    }

    public void StartAddTorque()
    {
        centerPos = Input.mousePosition;
    }

    public void StartAddTorque(Vector3 pos)
    {
        centerPos = pos;
    }

    public void StopAddTorque()
    {
        centerPos = Vector3.zero;
        lastPos = Vector3.zero;

        foreach (HingeJoint2D x in nodes)
        {
            x.useLimits = true;
        }

        rb.angularVelocity = 0;
        isMoving = false;
    }

    public void AddForceAtWeight(Vector3 pos) {
        StopAddTorque();

        Debug.Log("AddForceAtWeight " + pos.x + "," + pos.y);


        foreach (HingeJoint2D x in nodes)
        {
            x.transform.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            x.transform.GetComponent<Rigidbody2D>().angularVelocity =0;
        }
        hook.velocity = Vector3.zero;
        hook.angularVelocity = 0;
        //hook.mass = 500;
        //hook.velocity = (new Vector3(pos.x * force, pos.y * force, 0));
        //hook.velocity = new Vector2(pos.x * directionforce * hook.mass * 20, pos.y * directionforce * hook.mass * 20);

        hook.AddForce(new Vector2(pos.x * directionforce * hook.mass * 20, pos.y * directionforce * hook.mass * 20), ForceMode2D.Force);
    }

    public void SetSpeedToZero() {
        rb.angularVelocity = 0;
        rb.velocity = Vector2.zero;
        isMoving = false;
    }

    public void SetHingleArray(GameObject[] objlist)
    {
        Array.Clear(nodes, 0, nodes.Length);
        nodes = new HingeJoint2D[objlist.Length - 1];
        for (int i = 0; i < objlist.Length - 1; i++)
        {
            nodes[i] = objlist[i].GetComponent<HingeJoint2D>();
        }
    }

    private void FixedUpdate()
    {
        //Read Mouse Movement: clockwise or anticlockwise
        if (centerPos != Vector3.zero && lastPos != Vector3.zero)
        {
            //use radius of mouse position in last frame
            // r = sqrt(x^2 + y^2)
            float radius = Mathf.Sqrt(lastPos.x * lastPos.x + lastPos.y * lastPos.y);

            #region clockwise

            currRadian = RadianRelativeToCenter(Input.mousePosition, centerPos);
            radianDiff = Mathf.Abs(currRadian - lastRandian);

            // d = 2 * r * pi * lastDegree/360
            circularDisDiff = 0.01f * 2 * radius * Mathf.PI * Mathf.Rad2Deg * currRadian / 360;

            if (radianDiff > 0.03)
            {
                mouseStopCD = mouseStopTime;
                // clock movement if the mouse move larger than 5.7 degree
                if (currRadian > lastRandian)
                {
                    print("Clockwise");
                    // avoid the radian from 360 degree to 0 degree
                    if (lastGuestureMovement == 1)
                        ApplyTorque(-1 * circularDisDiff * acceleration);
                    //ApplyTorque(-1 * radianDiff * acceleration);

                    // Set last mouse guesture
                    lastGuestureMovement = 1;
                }
                else
                {
                    print("Counter Clockwise");

                    // avoid the radian from 360 degree to 0 degree
                    if (lastGuestureMovement == -1)
                        ApplyTorque(1 * circularDisDiff * acceleration);
                        //ApplyTorque(1 * radianDiff * acceleration);

                        // Set last mouse guesture
                        lastGuestureMovement = -1;
                }
            }
            else
            {
                //Stop Swing
                mouseStopCD = mouseStopCD - Time.fixedDeltaTime;
                if (mouseStopCD <= 0)
                    SetSpeedToZero();
            }
            
            lastRandian = currRadian;
            #endregion
        }

        if(centerPos != Vector3.zero)
            lastPos = PositionRelativeToCenter(Input.mousePosition, centerPos);


    }

    void ApplyTorque(float coefficent)
    {
        Debug.Log("ApplyTorque");
        isMoving = true;
        foreach (HingeJoint2D x in nodes)
        {
            x.useLimits = true;
        }

        rb.AddTorque(force * radius * coefficent);
        //rb.AddRelativeForce(currPos * force * radius * coefficent * 1000);
    }

    void ApplyForce()
    {
        foreach (HingeJoint2D x in nodes)
        {
            x.useLimits = false;
        }

        //Calcuate current tan
        currRadian = RadianRelativeToCenter(Input.mousePosition, centerPos);
        hookTan2 = RadianRelativeToCenter(hook.transform.position, rb.transform.position);

        Vector3 relativePos = new Vector3(Input.mousePosition.x - centerPos.x, Input.mousePosition.y - centerPos.y, 0);

        tanDiff = currRadian - hookTan2;
        
        if (currRadian > 0.1)
        {
            if (tanDiff > 0.1)
            {
                Debug.Log("ApplyForce Clockwise");
            }
            else if (tanDiff < -0.1)
            {
                Debug.Log("ApplyForce Anticlockwise");
            }
        }

        Vector3 direction = new Vector3(Mathf.Min(relativePos.x, directionforce), Mathf.Min(relativePos.y, directionforce));
        hook.AddForce(direction);
    }

    float RadianRelativeToCenter(Vector3 objPos, Vector3 centerPos)
    {

        Vector3 relativePos = new Vector3(objPos.x - centerPos.x, objPos.y - centerPos.y, 0);
        float radian = 0f;

        //4 Case: (x,y) | (x,-y) | (-x,-y) | (-x,y)
        if (relativePos.x > 0 && relativePos.y > 0)
            radian = Mathf.Atan2(relativePos.x, relativePos.y);
        else if (relativePos.x > 0 && relativePos.y < 0)
            radian = 90 * Mathf.Deg2Rad + Mathf.Atan2(-relativePos.y, relativePos.x);
        else if (relativePos.x < 0 && relativePos.y < 0)
            radian = 180 * Mathf.Deg2Rad + Mathf.Atan2(-relativePos.x, -relativePos.y);
        else if (relativePos.x < 0 && relativePos.y > 0)
            radian = 270f * Mathf.Deg2Rad + Mathf.Atan2(relativePos.y, -relativePos.x);

        return radian;
    }

    Vector3 PositionRelativeToCenter(Vector3 objPos, Vector3 centerPos) {
        return new Vector3(objPos.x - centerPos.x, objPos.y - centerPos.y, 0);
    }
}
