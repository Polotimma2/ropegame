﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HookView : MonoBehaviour
{
    public int numOfNode = 9;

    private WeightView weight;

    private Rigidbody2D centerRb; // torque center
    private HingeJoint2D[] nodes;

    private float radianDiff = 0; // radian difference of mouse position relative to the center point each frame

    private Vector3 centerPos = Vector3.zero;  // center point in screen view of clockwise movement
    private Vector3 currPos = Vector3.zero;  // current mouse position relative to the povit
    private Vector3 lastPos = Vector3.zero; // mouse position relative to the povit in the last frame;

    private float lastRandian = 0, currRadian = 0; // radian of last and current mouse position relative to the center point
    private float circularDisDiff = 0f; // circular distance of mopuse position between 2 frame

    private int lastGuestureMovement = 0; // 1 - clockwise, 2 - anticlockwise

    private float mouseStopCD = 0f;
    private float mouseStopTime = 0.3f;

    public bool IsSwinging { get { return _isSwinging; } }
    private bool _isSwinging = false;


    // Start is called before the first frame update
    private void Awake()
    {
        weight = FindObjectOfType<WeightView>();
        centerRb = transform.Find("center").GetComponent<Rigidbody2D>();

        nodes = new HingeJoint2D[numOfNode];
        for (int i = 1; i < numOfNode + 1; i++) {
            nodes[i - 1] = transform.Find("Node (" + i + ")").GetComponent<HingeJoint2D>();
        }
    }
    void Start()
    {
        mouseStopCD = mouseStopTime;
    }

    private void Update()
    {
        // set povit position
        if (GameManager.instance.state == PlayerState.Swing)
        {
            if (GameManager.instance.centerMode == MotionGestureCenterMode.Center)
            {
                //centerPos = GameCtrl.instance.player.transform.position;
                //Vector3 worldPos = Camera.main.ScreenToWorldPoint(centerPos);
                //GameCtrl.instance.povit.transform.position = new Vector3(worldPos.x, worldPos.y, 0);
            }
        }

        if (GameManager.instance.centerMode == MotionGestureCenterMode.Center)
        {
            //centerPos = GameCtrl.instance.player.transform.position;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(centerPos);
            GameManager.instance.povit.transform.position = new Vector3(worldPos.x, worldPos.y, 0);
        }
    }

    /// <summary>
    /// Start Add Torque with center at mouse position.
    /// </summary>
    public void StartAddTorque()
    {
        centerPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(centerPos);
        GameManager.instance.povit.transform.position = new Vector3(worldPos.x, worldPos.y, 0);
    }

    /// <summary>
    /// Start Add Torque with center at specificed position.
    /// </summary>
    /// <param name="pos">center position.</param>
    public void StartAddTorque(Vector3 pos)
    {
        centerPos = Camera.main.WorldToScreenPoint(pos);
        GameManager.instance.povit.transform.position = new Vector3(pos.x, pos.y, 0);
    }

    /// <summary>
    /// Stop add torque
    /// </summary>
    public void StopAddTorque()
    {
        _isSwinging = false;
        //centerPos = Vector3.zero;
        lastPos = Vector3.zero;

        centerRb.angularVelocity = 0;
    }

    /// <summary>
    /// Add Force to the weight with weight position relative to the center.
    /// </summary>
    /// <param name="center">center position.</param>
    public void AddForceAtWeightRelativeTo(Vector3 center)
    {

        // Stop add torque force
        StopAddTorque();

        // Calculate the force dircetion
        Vector3 direction = new Vector3(weight.transform.position.x - center.x, weight.transform.position.y - center.y, 0);

        // Reset the velocity of nodes, avoid affect on the AddForce Physics
        foreach (HingeJoint2D x in nodes)
        {
            x.transform.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            x.transform.GetComponent<Rigidbody2D>().angularVelocity = 0;
        }

        //SetSpeedToZero();

        // Add force to the weight
        if(GameManager.instance.comboMaxFlyBonus > 0)
            weight.AddForceAtWeight(direction, GameManager.instance.hookFlyForce * (1 + Mathf.Min(GameManager.instance.comboFlyBonus * GameManager.instance.player.flyCombo, GameManager.instance.comboMaxFlyBonus)));
        else
            weight.AddForceAtWeight(direction, GameManager.instance.hookFlyForce * (1 + GameManager.instance.comboFlyBonus * GameManager.instance.player.flyCombo));

    }

    /// <summary>
    /// Ignore weight's collision between ground & walls
    /// </summary>
    /// <param name="isOn"></param>
    public void IgnoreCollision(bool isOn)
    {
        weight.IgnoreCollision(isOn);
    }

    /// <summary>
    /// Set PlayerState of hook.
    /// </summary>
    /// <param name="state"></param>
    public void SetState(PlayerState state)
    {
        switch (state)
        {
            case PlayerState.Idle:
                StopAddTorque();
                IgnoreCollision(true);
                break;
            case PlayerState.Swing:

                // ignore collisions between hook and ground + wall
                weight.IgnoreCollision(true);

                // start add torque to hook
                if (GameManager.instance.centerMode == MotionGestureCenterMode.Mouse)
                    StartAddTorque();
                else if (GameManager.instance.centerMode == MotionGestureCenterMode.Player)
                    StartAddTorque(GameManager.instance.player.transform.position);
                else if (GameManager.instance.centerMode == MotionGestureCenterMode.Center)
                    StartAddTorque(Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0)));
                else if (GameManager.instance.centerMode == MotionGestureCenterMode.Custom)
                    StartAddTorque(GameManager.instance.customCenterPos);
                break;
            case PlayerState.Released:

                // reset velocity in order to avoid affects on the fly physics
                SetSpeedToZero();

                // open collisions between hook and ground + wall
                IgnoreCollision(false);
                break;
        }
    }

    private void FixedUpdate()
    {
        //Read Mouse Movement: clockwise or anticlockwise
        if (GameManager.instance.state == PlayerState.Swing && lastPos != Vector3.zero)
        {
            //use radius of mouse position in last frame
            // r = sqrt(x^2 + y^2)
            float radius = Mathf.Sqrt(lastPos.x * lastPos.x + lastPos.y * lastPos.y);

            #region clockwise

            currRadian = RadianRelativeToCenter(Input.mousePosition, centerPos);
            radianDiff = Mathf.Abs(currRadian - lastRandian);

            // d = 2 * r * pi * lastDegree/360
            circularDisDiff = 0.01f * 2 * radius * Mathf.PI * Mathf.Rad2Deg * currRadian / 360;

            if (radianDiff > 0.03)
            {
                mouseStopCD = mouseStopTime;
                // clock movement if the mouse move larger than 5.7 degree
                if (currRadian > lastRandian)
                {
                    // avoid the radian from 360 degree to 0 degree
                    if (lastGuestureMovement == 1)
                        ApplyTorque(-1 * circularDisDiff * GameManager.instance.hookTorqueAcc);
                    //ApplyTorque(-1 * radianDiff * acceleration);

                    // Set last mouse guesture
                    lastGuestureMovement = 1;
                }
                else
                {
                    // avoid the radian from 360 degree to 0 degree
                    if (lastGuestureMovement == -1)
                        ApplyTorque(1 * circularDisDiff * GameManager.instance.hookTorqueAcc);
                    //ApplyTorque(1 * radianDiff * acceleration);

                    // Set last mouse guesture
                    lastGuestureMovement = -1;
                }
            }
            else
            {
                //Stop Swing
                mouseStopCD = mouseStopCD - Time.fixedDeltaTime;
                if (mouseStopCD <= 0)
                    SetSpeedToZero();
            }

            lastRandian = currRadian;
            #endregion
        }

        if (GameManager.instance.state == PlayerState.Swing)
            lastPos = CommonFunction.GetRelativePos2D(Input.mousePosition, centerPos);

    }

    void ApplyTorque(float coefficent)
    {
        _isSwinging = true;
        // torque = force * radius * coeffiecnt * combo_bonus
        centerRb.AddTorque(GameManager.instance.hookTorqueForce * GameManager.instance.hookTorqueRadius * coefficent * (1 + GameManager.instance.comboAddOnTorque * GameManager.instance.player.flyCombo));
    }

    float RadianRelativeToCenter(Vector3 objPos, Vector3 centerPos)
    {

        Vector3 relativePos = new Vector3(objPos.x - centerPos.x, objPos.y - centerPos.y, 0);
        float radian = 0f;

        //4 Case: (x,y) | (x,-y) | (-x,-y) | (-x,y)
        if (relativePos.x > 0 && relativePos.y > 0)
            radian = Mathf.Atan2(relativePos.x, relativePos.y);
        else if (relativePos.x > 0 && relativePos.y < 0)
            radian = 90 * Mathf.Deg2Rad + Mathf.Atan2(-relativePos.y, relativePos.x);
        else if (relativePos.x < 0 && relativePos.y < 0)
            radian = 180 * Mathf.Deg2Rad + Mathf.Atan2(-relativePos.x, -relativePos.y);
        else if (relativePos.x < 0 && relativePos.y > 0)
            radian = 270f * Mathf.Deg2Rad + Mathf.Atan2(relativePos.y, -relativePos.x);

        return radian;
    }

    public void ResetNodesRotation()
    {
        transform.localRotation = Quaternion.Euler(0, 0, 90);
        int i = 0;
        foreach (HingeJoint2D x in nodes)
        {
            x.transform.localRotation = Quaternion.Euler(0, 0, 0);
            x.transform.localPosition = new Vector3(0, -0.03f - i * 0.3f, 0);
            i++;
        }
    }

    public void SetSpeedToZero()
    {
        _isSwinging = false;
        centerRb.angularVelocity = 0;
        centerRb.velocity = Vector2.zero;
    }
}
