﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

//auxiliar script
public class PlayerView : MonoBehaviour {

    public Rigidbody2D myRb { get { return _myRb; } }
    private Rigidbody2D _myRb;
    private SpriteRenderer playerSprite;
    //Orign Transform Data
    private Vector3 origPos;
    private Quaternion origRotation;
    private Vector3 origScale;

    private List<Item> collectedList;

    private Vector2 inputs = Vector2.zero;

    /// <summary>
    /// Indicates is the player at the ground.
    /// </summary>
    public bool isGroud { get { return _isGroud; } } // 
    private bool _isGroud = false;

    public int flyCombo { get { return _flyCombo; } }
    private int _flyCombo = 0;

    public bool isReadyToCombo { get { return _lastCOmboCD <= 0; } }
    public float _lastCOmboCD { get { return _lastComboCD; } }
    private float _lastComboCD = 0;

    private float addGravityCD = 0;

    /// <summary>
    /// Indicates is the player collided with wall and falling
    /// </summary>
    public bool isFalling { get { return _isFalling; } }
    private bool _isFalling = false;

    public Player player;

    public void Awake()
    {
        //Init
        collectedList = new List<Item>();
        playerSprite = GetComponent<SpriteRenderer>();

        //assigns rigidbody
        _myRb = GetComponent<Rigidbody2D>();

        // Set Orign Transform Data
        origPos = transform.position;
        origRotation = transform.rotation;
        origScale = transform.localScale;
    }

    public void ApplyPlayerSetting() {
        if(player == null)
            player = new Player(GameManager.instance.playerSpeed, GameManager.instance.playerGravity, GameManager.instance.playerMass);

        if(_myRb == null)
            _myRb = GetComponent<Rigidbody2D>();

        _myRb.gravityScale = player.gravity;
        _myRb.mass = player.mass;
    }

    private void Start()
    {
        gameObject.tag = "Player";
    }

    private void Update()
    {

        //flip player when is groud and not swinging the hook
        inputs = Vector2.zero;
        inputs.x = Input.GetAxis("Horizontal");

        if (_isGroud == true && GameManager.instance.hook.IsSwinging == false)
        {
            if (inputs.x < 0)
                transform.localScale = new Vector3(-origScale.x, origScale.y, 0);
            else if (inputs.x > 0)
                transform.localScale = new Vector3(origScale.x, origScale.y, 0);
        }

        if (_lastComboCD >= 0) {
            _lastComboCD -= Time.deltaTime;
        }
    }


    void FixedUpdate()
    {
        //move only if at the groud
        if (inputs != Vector2.zero && _isGroud == true)
            _myRb.MovePosition(_myRb.position + inputs * player.speed * Time.fixedDeltaTime);

        if (isFalling)
            playerSprite.color = Color.red;
        else
            playerSprite.color = Color.white;

        if (_isGroud == false) {
            addGravityCD -= Time.fixedDeltaTime;
            if (addGravityCD == 0) {
                _myRb.gravityScale = GameManager.instance.playerMaxGravity;
            }
        }
    }

    public void Reset()
    {
        SetSpeedToZero();
        transform.position = origPos;
        transform.rotation = origRotation;
    }

    public void SetSpeedToZero() {
        _myRb.velocity = Vector3.zero;
        _myRb.angularVelocity = 0;
    }

    public void SetRotationToZero() {
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void SetFalling(bool isF)
    {
        _isFalling = isF;
        if (isF)
        {
            //reset combo
            _flyCombo = 0; 
            _lastComboCD = 0;
        }
    }

    public void SetState(PlayerState state) {

        switch (state) {
            case PlayerState.Idle:
                _myRb.mass = 2000;
                _myRb.gravityScale = GameManager.instance.playerGravity;
                _lastComboCD = 0;
                break;
            case PlayerState.Swing:
                _myRb.mass = 2000;
                
                _myRb.angularVelocity = 0;
                _lastComboCD = 0;

                SetRotationToZero();
                break;
            case PlayerState.Released:

                _myRb.mass = player.mass;
                
                // reset velocity in order to avoid affects on the fly physics
                SetSpeedToZero();

                _lastComboCD = GameManager.instance.comboCD;
                addGravityCD = GameManager.instance.playerAddGravityTime;

                if (_isGroud)
                    _flyCombo = 0;
                else
                    _flyCombo++;
                break;
        }
    }
    

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Wall":
                SetFalling(true);

                // avoid duplicate call of collision enter
                if (!_isFalling)
                {

                    // if player collide wall when swing, fall now
                    if (GameManager.instance.state == PlayerState.Swing)
                    {
                        GameManager.instance.SetState(PlayerState.Released);
                    }
                }

                break;
            case "Ground":
                SetFalling(false);
                _flyCombo = 0;

                // reset gravity
                _myRb.gravityScale = GameManager.instance.playerGravity;

                // reset combo cd
                _lastComboCD = 0;

                // avoid duplicate call of collision enter
                if (!_isGroud)
                {
                    _isGroud = true;
                    // start swing mode
                    if (GameManager.instance.mouseCtrlMode == MouseControlMode.ClickToSwing)
                        GameManager.instance.SetState(PlayerState.Swing);
                    else
                    {
                        if (GameManager.instance.state != PlayerState.Swing)
                            // avoid rebound reset, reset to idle only the first time fall
                            GameManager.instance.SetState(PlayerState.Idle);
                    }
                }

                break;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        // if player collide wall at the ground
        if (_isFalling)
        {
            switch (collision.gameObject.tag)
            {
                case "Ground":
                    if (GameManager.instance.state == PlayerState.Released)
                    {
                        if (GameManager.instance.mouseCtrlMode == MouseControlMode.ClickToSwing)
                            GameManager.instance.SetState(PlayerState.Swing);
                        else
                            GameManager.instance.SetState(PlayerState.Idle);
                    }
                    SetFalling(false);
                    _myRb.freezeRotation = true ;
                    break;
            }
        }

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Ground":
                _isGroud = false;

                break;
        }
    }

    public void SetNextStartPoint(Vector3 savePoint)
    {
        origPos = savePoint;
    }

    public bool CheckHaveRequreItem(Item targetItem)
    {
        for (int i = 0; i < collectedList.Count; i++)
        {
            if (collectedList[i] == targetItem)
                return true;
        }

        return false;
    }
}
