﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainView : MonoBehaviour
{
    //surface group 
    private List<SurfaceFeature> _surfaces;
    

    private void Start()
    {
        // Find all surface features
        if (transform.GetComponent<SurfaceFeature>() != null)
            _surfaces.Add(transform.GetComponent<SurfaceFeature>());

        if (transform.GetComponent<SurfaceFeature>() == null)
        {
            foreach( SurfaceFeature sf in transform.GetComponentsInChildren<SurfaceFeature>()){
                _surfaces.Add(sf);
            }
        }


    }
}
