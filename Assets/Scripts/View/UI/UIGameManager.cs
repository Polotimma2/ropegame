﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameManager : MonoBehaviour
{
    public static UIGameManager instance { get { return _instance; } }
    private static UIGameManager _instance;

    private Canvas canvas;

    // info
    private Text txtInfo;

    private string txtCenterMode = "";
    private string txtCtrlMode = "";

    private Text txtCombo;
    private Text txtComboCD;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        // find objects
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();

        if(canvas.transform.Find("TxtCombo"))
            txtCombo = canvas.transform.Find("TxtCombo").GetComponent<Text>();

        if(canvas.transform.Find("TxtInfo"))
            txtInfo = canvas.transform.Find("TxtInfo").GetComponent<Text>();

        if (canvas.transform.Find("TxtComboCD"))
            txtComboCD = canvas.transform.Find("TxtComboCD").GetComponent<Text>();
    }

    public void Init()
    {
        UpdateCombo(0);
    }


    private void FixedUpdate()
    {
        if (GameManager.instance.centerMode == MotionGestureCenterMode.Player)
            txtCenterMode = "tab : change center (player)";
        else if (GameManager.instance.centerMode == MotionGestureCenterMode.Mouse)
            txtCenterMode = "tab : change center (mouse)";
        else if (GameManager.instance.centerMode == MotionGestureCenterMode.Center)
            txtCenterMode = "tab : change center (Center)";
        else if (GameManager.instance.centerMode == MotionGestureCenterMode.Custom)
            txtCenterMode = "tab : change center (Custom)";

        if (GameManager.instance.mouseCtrlMode == MouseControlMode.ClickToSwing)
            txtCtrlMode = "  m : change control (click)";
        else
            txtCtrlMode = "  m : change control (hold)";


        txtInfo.text = "    r : reset\n" + txtCenterMode + "\n" + txtCtrlMode;

        UpdateCombo(GameManager.instance.player.flyCombo);

        UpdateComboCD(GameManager.instance.player._lastCOmboCD);
    }

    public void UpdateCombo(int combo) {
        if (txtCombo == null) return;

        if (combo > 0)
            txtCombo.text = "COMBO " + combo;
        else
            txtCombo.text = "";

    }

    public void UpdateComboCD(float cd) {
        if (txtComboCD == null) return;

        if (cd > 0)
            txtComboCD.text = cd.ToString();
        else
            txtComboCD.text = "";
    }
}
