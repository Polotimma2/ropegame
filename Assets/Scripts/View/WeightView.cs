﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightView : MonoBehaviour
{
    private Rigidbody2D myRb;

    private void Awake()
    {
        myRb = GetComponent<Rigidbody2D>();
    }

    // add force to rigibody
    public void AddForceAtWeight(Vector2 direction, float force)
    {

        myRb.velocity = Vector3.zero;
        myRb.angularVelocity = 0;
        //myRb.mass = 180;
        //hook.velocity = (new Vector3(pos.x * force, pos.y * force, 0));
        //hook.velocity = new Vector2(pos.x * directionforce * hook.mass * 20, pos.y * directionforce * hook.mass * 20);
        myRb.AddForce(new Vector2(direction.x * force * myRb.mass * 20, direction.y * force * myRb.mass * 20), ForceMode2D.Force);
    }

    // Ingore Collision of grounds and walls
    public void IgnoreCollision(bool isOn)
    {
        Physics2D.IgnoreLayerCollision(8, 9, isOn);
        Physics2D.IgnoreLayerCollision(8, 10, isOn);
    }

    // Update Rigidbody Mass
    public void SetMass(float mass) {
        myRb.mass = mass;
    }

    public void SetGravity(float gravity) {
        myRb.gravityScale = gravity;
    }

    public void SetVelocityToZero() {
        //myRb.velocity = Vector2.zero;
        myRb.angularVelocity = 0;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("OnCollisionEnter2D: " + collision.gameObject.name);
        switch (collision.gameObject.tag)
        {
            case "Wall":
                GameManager.instance.player.SetFalling(true);
                break;
            case "Ground":
                //if(GameCtrl.instance.player.isGroud == false)
                //    GameCtrl.instance.player.SetFalling(true);
                break;
        }
    }
}
