﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Sample
{
    public class RopeScript : MonoBehaviour
    {

        //holds where the hook is going to
        [HideInInspector]
        public Vector2 destiny;

        //distance between each node
        public float distance = 2;

        //max nodes
        public int maxNodes = 20;

        //node prefab
        public GameObject nodePrefab;

        //player gameobject
        public GameObject player;

        //last node instantiated
        public GameObject lastNode;

        //line that represents rope
        LineRenderer lr;

        //initial points on the rope (beginning and end)
        int vertexCount = 2;

        //list of all nodes instantiated
        public List<GameObject> Nodes = new List<GameObject>();

        //check if the full rope is created
        bool done = false;

        //is something if an object with a rigidbody is hit
        Transform target;

        //added hinge joint if there is relative object
        HingeJoint2D hinge;

        //rope head rig2d
        Rigidbody2D rig;

        //throwing 
        bool throwing;

        // Use this for initialization
        void Start()
        {

            //sets the line renderer
            lr = GetComponent<LineRenderer>();

            //sets the Rigidbody2D
            rig = GetComponent<Rigidbody2D>();

            //sets player
            if (player == null)
                player = GameObject.FindGameObjectWithTag("Player").gameObject;

            //sets last node to the hook
            lastNode = transform.gameObject;

            //add it to nodelist
            Nodes.Add(transform.gameObject);

            //prevents game from freezing if distance is zero
            if (distance == 0)
            {
                distance = 1;
            }

            throwing = false;

        }

        // Update is called once per frame
        void Update()
        {

            //while hook is not on destiny
            if (done == false)
            {

                if (throwing)
                {
                    while (Vector2.Distance(player.transform.position, lastNode.transform.position) > distance * 2f && !done)
                    {
                        CreateNode();
                    }
                }
                else
                {
                    //set it to done
                    done = true;
                    //creates node between last node and player (in the same frame)
                    while (Vector2.Distance(player.transform.position, lastNode.transform.position) > distance)
                    {
                        CreateNode();
                    }
                }

                //enables joint to move with object(happens only if target is not null)
                if (hinge != null)
                    hinge.autoConfigureConnectedAnchor = false;

                //binds last node to player

                if (!throwing)
                {
                    lastNode.GetComponent<HingeJoint2D>().connectedBody = player.GetComponent<Rigidbody2D>();
                    lastNode.GetComponent<HingeJoint2D>().useLimits = false;
                }
            }

            RenderLine();
        }

        public void ThrowRope()
        {
            Debug.Log("ThrowRope lastNode: " + lastNode.name);
            lastNode.GetComponent<HingeJoint2D>().enabled = false;
            throwing = true;
            done = false;
            CommonFunction.call.DelayFunc(() =>
            {
                IgnoreCollision(false);
            }, 0.1f);
        }

        public void ReconnectRope(Vector3 _destiny)
        {
            Freeze();
            foreach (GameObject n in Nodes)
            {
                if (n != gameObject)
                    Destroy(n);
            }
            vertexCount = 2;
            lr.positionCount = 0;

            Nodes = new List<GameObject>();
            Nodes.Add(gameObject);
            lastNode = transform.gameObject;
            lastNode.GetComponent<HingeJoint2D>().enabled = true;

            SetPosition(_destiny);
            done = false;

        }

        public void CatchRope()
        {
            done = true;
            rig.isKinematic = false;
            throwing = false;
            IgnoreCollision(true);
            lastNode.GetComponent<HingeJoint2D>().enabled = true;
        }

        public void CollapseRopeNode()
        {
            Debug.LogWarning("CollapseRopeNode: " + lastNode.name);
            if (lastNode != gameObject && vertexCount > 2)
            {
                Nodes.Remove(lastNode);
                Destroy(lastNode);
                lastNode = Nodes[Nodes.Count - 1];
                lastNode.GetComponent<HingeJoint2D>().connectedBody = null;
                vertexCount--;
            }
            else
            {
                while (Vector2.Distance(player.transform.position, lastNode.transform.position) > distance)
                {
                    CreateNode();
                }
            }
        }


        public void SetPosition(Vector3 _destiny)
        {
            //if (transform.position != (Vector3)destiny && !done)
            //	transform.position = Vector2.MoveTowards(transform.position, destiny, speed * Time.deltaTime);

            destiny = _destiny;
            transform.position = (Vector3)destiny;
        }

        //renders rope
        void RenderLine()
        {
            if (vertexCount > 2)
            {
                int i = 0;
                //sets vertex count of rope
                lr.positionCount = vertexCount;
                //each node is a vertex oft the rope
                for (i = 0; i < Nodes.Count; i++)
                {
                    if (Nodes[i] != null)
                        lr.SetPosition(i, Nodes[i].transform.position);
                }

                //sets last vetex of rope to be the player
                lr.SetPosition(i, player.transform.position);
            }
            else
            {
                lr.positionCount = 0;
            }
        }

        public void SetDone(bool _done)
        {
            done = _done;
        }

        void CreateNode()
        {
            //	Debug.Log("CreateNode: " + vertexCount);
            if (vertexCount < maxNodes)
            {
                //makes vector that points from last node to player
                Vector2 pos2Create = player.transform.position - lastNode.transform.position;

                //makes it desired lenght
                pos2Create.Normalize();
                pos2Create *= distance;

                //adds lastnode's position to that node
                pos2Create += (Vector2)lastNode.transform.position;

                //instantiates node at that position
                GameObject go = (GameObject)Instantiate(nodePrefab, pos2Create, Quaternion.identity);
                go.name = "Node" + vertexCount;
                //sets parent to be this hook
                //go.transform.SetParent(transform);

                //sets hinge joint from last node to connect to this node
                lastNode.GetComponent<HingeJoint2D>().connectedBody = go.GetComponent<Rigidbody2D>();
                lastNode.GetComponent<HingeJoint2D>().enabled = true;
                lastNode.GetComponent<HingeJoint2D>().useLimits = true;

                go.GetComponent<HingeJoint2D>().enabled = !throwing;
                go.GetComponent<HingeJoint2D>().useLimits = true;

                //if attached to an object, turn of colliders (you may want this to be deleted in some cases)
                if (target != null && go.GetComponent<Collider2D>() != null)
                {
                    go.GetComponent<Collider2D>().enabled = false;
                }


                //sets this node as the last node instantiated
                lastNode = go;

                //adds node to node list
                Nodes.Add(lastNode);

                //increases number of nodes / vertices
                vertexCount++;
            }
            else
            {
                done = true;
                lastNode.GetComponent<HingeJoint2D>().enabled = true;
            }
        }

        void OnCollisionEnter2D(Collision2D col)
        {
            Debug.LogWarning("OnCollisionEnter2D: " + col.gameObject.name);
            if (col.transform.tag == "Wall")
            {
                if (throwing)
                    Freeze();
            }
        }

        void Freeze()
        {
            throwing = false;
            done = true;
            rig.velocity = Vector3.zero;
            rig.angularVelocity = 0;
            rig.isKinematic = true;
            lastNode.GetComponent<HingeJoint2D>().enabled = true;
        }

        public void IgnoreCollision(bool isOn)
        {
            Physics2D.IgnoreLayerCollision(8, 9, isOn);
            Physics2D.IgnoreLayerCollision(8, 10, isOn);
        }

    }
}
