﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeScriptBalance : MonoBehaviour
{
 
    //player gameobject
    public GameObject player;

    //last node instantiated
    public GameObject lastNode;

    //list of all nodes instantiated
    public List<GameObject> Nodes = new List<GameObject>();

    //rope head rig2d
    Rigidbody2D rbHook;


    // Use this for initialization
    void Start()
    {


        //sets the Rigidbody2D
        rbHook = GetComponent<Rigidbody2D>();


        //sets last node to the hook
        lastNode = transform.gameObject;

        //add it to nodelist
        Nodes.Add(transform.gameObject);


    }


    public void IgnoreCollision(bool isOn)
    {
        Physics2D.IgnoreLayerCollision(8, 9, isOn);
        Physics2D.IgnoreLayerCollision(8, 10, isOn);
    }
}
